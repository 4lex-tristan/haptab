<?php 
class module_choixmitigation extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		$this->oLayout=new _layout('bootstrap');
		
		$this->oLayout->addModule('menu','menuSimulation::index');
	}
	
	
	public function _index(){
	if (model_parametrevalidation::getInstance()->riskIsValidated($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi']) == true){
			_root::redirect('choixmitigation::detail&suivi=' . $_SESSION['suivi'] );
	    }elseif (model_choixmitigation::getInstance()->isFulfilled($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi']) == true)
	    {
	    	_root::redirect('choixmitigation::editlist&suivi=' . $_SESSION['suivi']);
	    }else{
	    	$this->_new();
	    }
	}
	
	
	public function _list(){
		
		$tChoixmitigation=model_choixmitigation::getInstance()->findAll();
		
		$oView=new _view('choixmitigation::list');
		$oView->tChoixmitigation=$tChoixmitigation;
		
				$oView->tJoinmodel_mitigationrisque=model_mitigationrisque::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		if ($_SESSION ['suivi'] == - 1) {
			$oView = new _view ( 'choixmitigation::nosuivi' );
		} else {
		$tMessage=$this->processSave();

		$tMitigations = array();
		
		$tRisques=model_risque::getInstance()->findAllBySimu($_SESSION['simulation']);
		foreach ($tRisques as $oRisque){
			$tMitigations[]=model_mitigationrisque::getInstance()->getSelectMitig($oRisque->id);
		}
		
	
		$oChoixmitigation=new row_choixmitigation;
		
		$oView=new _view('choixmitigation::new');
		$oView->oChoixmitigation=$oChoixmitigation;
		$oView->tMitigations=$tMitigations;
		$oView->tRiques=$tRisques;
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		}
		
		$this->oLayout->add('main',$oView);
	}

	public function _editlist(){
		if ($_SESSION ['suivi'] == - 1) {
			$oView = new _view ( 'choixmitigation::nosuivi' );
		} else {
		$tMessage=$this->ProcessValidation();
	
		$tRisques=model_risque::getInstance()->findAllBySimu($_SESSION['simulation']);
		

	
		$oView=new _view('choixmitigation::editlist');
		
		$oView->tRisques=$tRisques;
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		}
		
		$this->oLayout->add('main',$oView);
	}
	
	public function _edit(){
		
		$tMessage=$this->processSave();
		
		$oChoixmitigation=model_choixmitigation::getInstance()->findById( _root::getParam('id') );
		$oRisque=model_risque::getInstance()->findById($oChoixmitigation->id_risque);
		$oMitigation = model_mitigationrisque::getInstance()->findById($oChoixmitigation->id_mitigation);
		
		$oView=new _view('choixmitigation::edit');
		$oView->oChoixmitigation=$oChoixmitigation;
		$oView->oRisque=$oRisque;
		
		$oView->tJoinmodel_mitigationrisque=model_mitigationrisque::getInstance()->getSelectMitig($oChoixmitigation->id_risque);		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	public function _detail(){
		$tRisques=model_risque::getInstance()->findAllBySimu($_SESSION['simulation']);
		if (_root::getParam ( 'id' )){
			// jamais un param id est utilisé pour choixmitigation::detail
			foreach ($tRisques as $oRisque){
				$oChoixMitigations=model_choixmitigation::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi'],$oRisque->id);
				$oMitigation=model_mitigationrisque::getInstance()->findById($oChoixMitigations->id_mitigation);
				$tMitigations[]=$oMitigation->intitule;
			}
		}elseif (_root::getParam('suivi')){
			foreach ($tRisques as $oRisque){
				$oChoixMitigations=model_choixmitigation::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],_root::getParam('suivi'),$oRisque->id);
				$oMitigation=model_mitigationrisque::getInstance()->findById($oChoixMitigations->id_mitigation);
				$tMitigations[]=$oMitigation->intitule;
			}
		}
		
		
	
	
		$oView=new _view('choixmitigation::detail');
		$oView->tMitigations=$tMitigations;
		$oView->tRisques=$tRisques;
		$this->oLayout->add('main',$oView);
	}
	
	public function _show(){
		$oChoixmitigation=model_choixmitigation::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('choixmitigation::show');
		$oView->oChoixmitigation=$oChoixmitigation;
		
		$oView->tJoinmodel_mitigationrisque=model_mitigationrisque::getInstance()->getSelect();
		$this->oLayout->add('main',$oView);
	}

	
	

	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
		
		$nbTachesSoumises = sizeof($_POST['id_mitigation']); 
		for($i =0; $i<=$nbTachesSoumises-1;$i++){
			$iId=_root::getParam('id',null); // si c'est une modification, il y a un numero d'identification pour le choix de mitigation
			if($iId==null){
				$oChoixmitigation=new row_choixmitigation;	
				$oChoixmitigation->id_simu=$_SESSION['simulation'];
				$oChoixmitigation->id_equipe=$_SESSION['nEquipe'];
				$oChoixmitigation->id_suivi=$_SESSION['suivi'];
				$tColumn=array('id_risque', 'id_mitigation');
				foreach($tColumn as $sColumn){
					$oChoixmitigation->$sColumn=_root::getParam($sColumn)[$i] ;
				}
			}else{
				$oChoixmitigation=model_choixmitigation::getInstance()->findById( _root::getParam('id',null) );
			}

			//appel a fonction pour durée 'DureeCalcule'
			$tColumn=array('id_mitigation');
			foreach($tColumn as $sColumn){
				$oChoixmitigation->$sColumn=_root::getParam($sColumn)[$i] ;
			}
				
			if($oChoixmitigation->save()){
				
			}else {
				return $oChoixmitigation->getListError();
			}
		}
		//une fois l'ensemble des mitigations enregistrees, on redirige vers la page indix qui redirige en fonction de la validation
		_root::redirect('choixmitigation::index');
	}
	
	private function ProcessValidation(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne valide pas 
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
		
		if (model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
			$oValidation = model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		} else {
			$oValidation = new row_parametrevalidation ();
		}
		$oValidation->id_equipe = $_SESSION ['nEquipe'];
		$oValidation->id_simu = $_SESSION ['simulation'];
		$oValidation->id_suivi = $_SESSION ['suivi'];
		$oValidation->risque_valid = 1;
		if ($oValidation->save ()) {
			_root::redirect('choixmitigation::detail',array('suivi'=>$_SESSION['suivi']));
		} else {
			return $oValidation->getListError ();
		}
		
	}

	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

