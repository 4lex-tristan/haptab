<form  class="form-horizontal" >
<?php 
$i=0;
foreach ($this->tRisques as $oRisque):?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label"><?php echo $oRisque->nom;?></label>
		<div class="col-sm-8"><?php 
		echo $this->tMitigations[$i];?></div>
	</div>

<?php $i++;
endforeach;?>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<a class="btn btn-link" href="<?php echo $this->getLink('tdbGroupe::index')?>">Retour</a>
	</div>
</div>
</form>