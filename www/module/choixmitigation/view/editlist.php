<?php 
$oForm=new plugin_form();
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >
<?php 
$i=0;
foreach ($this->tRisques as $oRisque):?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label"><?php echo $oRisque->nom;?></label>
		<div class="col-sm-8"><?php $oChoixMitigations=model_choixmitigation::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi'],$oRisque->id);
		$oMitigations=model_mitigationrisque::getInstance()->findById($oChoixMitigations->id_mitigation);
		echo $oMitigations->intitule;
		//echo' <input type="hidden" name="id_risque['.$i.']" value="'.$oRisque->id_risque.'" />';?></div>
		<div class="col-sm-2"><a class="btn btn-warning" href="<?php echo $this->getLink('choixmitigation::edit',array('id'=>$oRisque->id) 	)?>">Modifier</a></div>
	</div>

<?php $i++;
endforeach;?>
<?php echo $oForm->getToken('token',$this->token)?>
<input type="hidden" name="validate" value="True" />

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<input type="submit" class="btn btn-success" >
	</div>
</div>
</form>

