<?php 
$oForm=new plugin_form($this->oChoixmitigation);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >
<?php 
$i=0;
foreach ($this->tRiques as $oRisque):?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label"><?php echo $oRisque->nom;?></label>
		<div class="col-sm-8"><?php 
		echo $oForm->getSelect('id_mitigation['.$i.']',$this->tMitigations[$i],array('class'=>'form-control'));
		echo' <input type="hidden" name="id_risque['.$i.']" value="'.$oRisque->id.'" />';?></div>
	</div>

<?php $i++;
endforeach;?>
<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Soumettre" /> 
	</div>
</div>
</form>
