<table class="table table-striped">
	<tr>
		
		<th>Mitigation</th>

		<th></th>
	</tr>
	<?php if($this->tChoixmitigation):?>
		<?php foreach($this->tChoixmitigation as $oChoixmitigation):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php if(isset($this->tJoinmodel_mitigationrisque[$oChoixmitigation->id_mitigation])){ echo $this->tJoinmodel_mitigationrisque[$oChoixmitigation->id_mitigation];}else{ echo $oChoixmitigation->id_mitigation ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('choixmitigation::edit',array(
										'id'=>$oChoixmitigation->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-default" href="<?php echo $this->getLink('choixmitigation::show',array(
										'id'=>$oChoixmitigation->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="2">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('choixmitigation::new') ?>">New</a></p>

