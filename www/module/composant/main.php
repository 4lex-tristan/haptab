<?php 
class module_composant extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
		
		//$this->oLayout->addModule('menu','menu::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tComposant=model_composant::getInstance()->findAll();
		
		$oView=new _view('composant::list');
		$oView->tComposant=$tComposant;
		
				$oView->tJoinmodel_variationsqualite=model_variationsqualite::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oComposant=new row_composant;
		
		$oView=new _view('composant::new');
		$oView->oComposant=$oComposant;
		
				$oView->tJoinmodel_variationsqualite=model_variationsqualite::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oComposant=model_composant::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('composant::edit');
		$oView->oComposant=$oComposant;
		$oView->tId=model_composant::getInstance()->getIdTab();
		
				$oView->tJoinmodel_variationsqualite=model_variationsqualite::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _show(){
		$oComposant=model_composant::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('composant::show');
		$oView->oComposant=$oComposant;
		
				$oView->tJoinmodel_variationsqualite=model_variationsqualite::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oComposant=model_composant::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('composant::delete');
		$oView->oComposant=$oComposant;
		
				$oView->tJoinmodel_variationsqualite=model_variationsqualite::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oComposant=new row_composant;	
		}else{
			$oComposant=model_composant::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('nom','type','poids','puissance','duree_sup_proto','cout_unit_proto','duree_sup_prod','cout_unit_prod','point_qualite','niveau_qualite','qualite_hasard','id_simulation');
		foreach($tColumn as $sColumn){
			$oComposant->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oComposant->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('composant::list');
		}else{
			return $oComposant->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oComposant=model_composant::getInstance()->findById( _root::getParam('id',null) );
				
		$oComposant->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('composant::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

