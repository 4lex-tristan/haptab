<form class="form-horizontal" action="" method="POST" >
	
	<div class="form-group">
		<label class="col-sm-2 control-label">nom</label>
		<div class="col-sm-10"><?php echo $this->oComposant->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">type</label>
		<div class="col-sm-10"><?php echo $this->oComposant->type ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">poids</label>
		<div class="col-sm-10"><?php echo $this->oComposant->poids ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">puissance</label>
		<div class="col-sm-10"><?php echo $this->oComposant->puissance ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">dur&eacute;e supp. proto.</label>
		<div class="col-sm-10"><?php echo $this->oComposant->duree_sup_proto ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">cout unitaire proto.</label>
		<div class="col-sm-10"><?php echo $this->oComposant->cout_unit_proto ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">dur&eacute;e supp. prod.</label>
		<div class="col-sm-10"><?php echo $this->oComposant->duree_sup_prod ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">cout unitaire prod.</label>
		<div class="col-sm-10"><?php echo $this->oComposant->cout_unit_prod ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">point de qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $this->oComposant->point_qualite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_variationsqualite[$this->oComposant->niveau_qualite]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">qualit&eacute; hasard</label>
		<div class="col-sm-10"><?php echo $this->oComposant->qualite_hasard ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oComposant->id_simulation]?></div>
	</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		 <a class="btn btn-default" href="<?php echo $this->getLink('composant::list')?>">Retour</a>
	</div>
</div>
</form>
