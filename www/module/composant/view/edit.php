<?php 
$oForm=new plugin_form($this->oComposant);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">nom</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">type</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('type',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">poids</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('poids',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">puissance</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('puissance',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">dur&eacute;e supp. proto.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('duree_sup_proto',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">cout unitaire proto.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('cout_unit_proto',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">dur&eacute;e supp. prod.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('duree_sup_prod',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">cout unitaire prod.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('cout_unit_prod',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">point de qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('point_qualite',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('niveau_qualite',$this->tJoinmodel_variationsqualite,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">qualit&eacute; hasard</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite_hasard',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_simulation',$this->tJoinmodel_simulation,array('class'=>'form-control'));?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('composant::list')?>">Annuler</a>
	</div>
</div>
</form>

