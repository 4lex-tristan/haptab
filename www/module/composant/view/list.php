<table class="table table-striped">
	<tr>
		
		<th>nom</th>

		<th>type</th>

		<th>poids</th>

		<th>puissance</th>

		<th>dur&eacute;e supp. proto.</th>

		<th>cout unitaire proto.</th>

		<th>dur&eacute;e supp. prod.</th>

		<th>cout unitaire prod.</th>

		<th>point de qualit&eacute;</th>

		<th>niveau qualit&eacute;</th>

		<th>qualit&eacute; hasard</th>

		<th>simulation</th>

		<th></th>
	</tr>
	<?php if($this->tComposant):?>
		<?php foreach($this->tComposant as $oComposant):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oComposant->nom ?></td>

		<td><?php echo $oComposant->type ?></td>

		<td><?php echo $oComposant->poids ?></td>

		<td><?php echo $oComposant->puissance ?></td>

		<td><?php echo $oComposant->duree_sup_proto ?></td>

		<td><?php echo $oComposant->cout_unit_proto ?></td>

		<td><?php echo $oComposant->duree_sup_prod ?></td>

		<td><?php echo $oComposant->cout_unit_prod ?></td>

		<td><?php echo $oComposant->point_qualite ?></td>

		<td><?php if(isset($this->tJoinmodel_variationsqualite[$oComposant->niveau_qualite])){ echo $this->tJoinmodel_variationsqualite[$oComposant->niveau_qualite];}else{ echo $oComposant->niveau_qualite ;}?></td>

		<td><?php echo $oComposant->qualite_hasard ?></td>

		<td><?php if(isset($this->tJoinmodel_simulation[$oComposant->id_simulation])){ echo $this->tJoinmodel_simulation[$oComposant->id_simulation];}else{ echo $oComposant->id_simulation ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('composant::edit',array(
										'id'=>$oComposant->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('composant::delete',array(
										'id'=>$oComposant->getId()
									) 
							)?>">Delete</a>

<a class="btn btn-default" href="<?php echo $this->getLink('composant::show',array(
										'id'=>$oComposant->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="13">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('composant::new') ?>">New</a></p>

