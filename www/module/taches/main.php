<?php 
class module_taches extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tTaches=model_taches::getInstance()->findAll();
		
		$oView=new _view('taches::list');
		$oView->tTaches=$tTaches;
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oTaches=new row_taches;
		
		$oView=new _view('taches::new');
		$oView->oTaches=$oTaches;
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oTaches=model_taches::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('taches::edit');
		$oView->oTaches=$oTaches;
		$oView->tId=model_taches::getInstance()->getIdTab();
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oTaches=model_taches::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('taches::delete');
		$oView->oTaches=$oTaches;
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oTaches=new row_taches;	
		}else{
			$oTaches=model_taches::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('Nom','Pays','Duree_initiale','Nb_Ing_Init','Nb_Tech_Init','Nb_Ouv_Init','id_simu');
		foreach($tColumn as $sColumn){
			$oTaches->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oTaches->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('taches::list');
		}else{
			return $oTaches->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oTaches=model_taches::getInstance()->findById( _root::getParam('id',null) );
				
		$oTaches->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('taches::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

