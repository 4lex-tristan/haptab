<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Nom</label>
		<div class="col-sm-10"><?php echo $this->oTaches->Nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Pays</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_contraintepays[$this->oTaches->Pays]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Dur&eacute;e initiale</label>
		<div class="col-sm-10"><?php echo $this->oTaches->Duree_initiale ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Ing. initial</label>
		<div class="col-sm-10"><?php echo $this->oTaches->Nb_Ing_Init ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Tech. initial</label>
		<div class="col-sm-10"><?php echo $this->oTaches->Nb_Tech_Init ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Ouv. initial</label>
		<div class="col-sm-10"><?php echo $this->oTaches->Nb_Ouv_Init ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oTaches->id_simu]?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('taches::list')?>">Annuler</a>
	</div>
</div>
</form>
