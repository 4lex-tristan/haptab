<table class="table table-striped">
	<tr>
		
		<th>Nom</th>

		<th>Pays</th>

		<th>Dur&eacute;e initiale</th>

		<th>Nb. Ing. initial</th>

		<th>Nb. Tech. initial</th>

		<th>Nb. Ouv. initial</th>

		<th>Simulation</th>

		<th></th>
	</tr>
	<?php if($this->tTaches):?>
		<?php foreach($this->tTaches as $oTaches):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oTaches->Nom ?></td>

		<td><?php if(isset($this->tJoinmodel_contraintepays[$oTaches->Pays])){ echo $this->tJoinmodel_contraintepays[$oTaches->Pays];}else{ echo $oTaches->Pays ;}?></td>

		<td><?php echo $oTaches->Duree_initiale ?></td>

		<td><?php echo $oTaches->Nb_Ing_Init ?></td>

		<td><?php echo $oTaches->Nb_Tech_Init ?></td>

		<td><?php echo $oTaches->Nb_Ouv_Init ?></td>

		<td><?php if(isset($this->tJoinmodel_simulation[$oTaches->id_simu])){ echo $this->tJoinmodel_simulation[$oTaches->id_simu];}else{ echo $oTaches->id_simu ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('taches::edit',array(
										'id'=>$oTaches->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('taches::delete',array(
										'id'=>$oTaches->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="8">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('taches::new') ?>">New</a></p>

