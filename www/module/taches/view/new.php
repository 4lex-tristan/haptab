<?php 
$oForm=new plugin_form($this->oTaches);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Pays</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('Pays',$this->tJoinmodel_contraintepays,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Dur&eacute;e initiale</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Duree_initiale',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Ing. initial</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Nb_Ing_Init',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Tech. initial</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Nb_Tech_Init',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. Ouv. initial</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Nb_Ouv_Init',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_simu',$this->tJoinmodel_simulation,array('class'=>'form-control'));?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('taches::list')?>">Annuler</a>
	</div>
</div>
</form>
