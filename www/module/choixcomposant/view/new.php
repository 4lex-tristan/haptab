<?php 
$oForm=new plugin_form($this->oChoixcomposant);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Ecran</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_ecran',$this->tJoinmodel_ComposantEcran,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Processeur</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_processeur',$this->tJoinmodel_ComposantProcesseur,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Memoire</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_memoire',$this->tJoinmodel_ComposantMemoire,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Connexion sans fil</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_wireless',$this->tJoinmodel_ComposantWireless,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Batterie</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_battrie',$this->tJoinmodel_ComposantBattrie,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cam&eacute;ra</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_camera',$this->tJoinmodel_ComposantCamera,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Webcam</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_webcam',$this->tJoinmodel_ComposantWebcam,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Gps</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_gps',$this->tJoinmodel_ComposantGPS,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Clavier</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_clavier',$this->tJoinmodel_ComposantClavier,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Boitier</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_boitier',$this->tJoinmodel_ComposantBoitier,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Systeme d'exploitation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_os',$this->tJoinmodel_OS,array('class'=>'form-control'))?></div>
	</div>

<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Soumettre" />
	</div>
</div>
</form>
