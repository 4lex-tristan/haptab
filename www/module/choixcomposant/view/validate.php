<?php 
$oForm=new plugin_form();
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Ecran</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantEcran[$this->oChoixcomposant->id_ecran] ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Processeur</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantProcesseur[$this->oChoixcomposant->id_processeur]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Memoire</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantMemoire[$this->oChoixcomposant->id_memoire]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Connexion sans fil</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantWireless[$this->oChoixcomposant->id_wireless]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Batterie</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantBattrie[$this->oChoixcomposant->id_battrie]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cam&eacute;ra</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantCamera[$this->oChoixcomposant->id_camera]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Webcam</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantWebcam[$this->oChoixcomposant->id_webcam]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Gps</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantGPS[$this->oChoixcomposant->id_gps]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Clavier</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantClavier[$this->oChoixcomposant->id_clavier]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Boitier</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_ComposantBoitier[$this->oChoixcomposant->id_boitier]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Systeme d'exploitation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_OS[$this->oChoixcomposant->id_os]?></div>
	</div>
	<?php echo $oForm->getToken('token',$this->token)?>
	<input type="hidden" name="validate" value="True" />
	<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<input type="submit" class="btn btn-success">
		<a class="btn btn-warning" href="<?php echo $this->getLink('choixcomposant::edit',array('id'=>$this->oChoixcomposant->id))?>">Modifier</a>
	</div>
</div>
</form>