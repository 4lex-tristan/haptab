<?php 
class module_choixcomposant extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		$this->oLayout=new _layout('bootstrap');

		$this->oLayout->addModule('menu','menuSimulation::index');
	}
	
	
	public function _index(){
			if (model_parametrevalidation::getInstance ()->tabletIsValidated ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				$oTablette = model_choixcomposant::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
				_root::redirect ( 'choixcomposant::detail&id=' . $oTablette->id );
			} elseif (model_choixcomposant::getInstance ()->isFulfilled ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				$oTablette = model_choixcomposant::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
				_root::redirect ( 'choixcomposant::validate&id=' . $oTablette->id );
			} else {
				$this->_new ();
			}
	}
	
	
	public function _validate(){
		$tMessage=$this->ProcessValidation();
		if(_root::getParam('id') != NULL){
			$oChoixcomposant=model_choixcomposant::getInstance()->findById( _root::getParam('id') );
		}elseif(_root::getParam('suivi') != NULL){
			$oChoixcomposant = model_choixcomposant::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], _root::getParam('suivi') );
		}
		else{
			$oChoixcomposant = model_choixcomposant::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		}
		
		$oView=new _view('choixcomposant::validate');
		$oView->oChoixcomposant=$oChoixcomposant;
			$oView->tJoinmodel_ComposantEcran=model_composant::getInstance()->getSelectEcran();
			$oView->tJoinmodel_ComposantProcesseur=model_composant::getInstance()->getSelectProcesseur();
			$oView->tJoinmodel_ComposantMemoire=model_composant::getInstance()->getSelectMemoire();
			$oView->tJoinmodel_ComposantWireless=model_composant::getInstance()->getSelectWireless();
			$oView->tJoinmodel_ComposantBattrie=model_composant::getInstance()->getSelectBatterie();
			$oView->tJoinmodel_ComposantCamera=model_composant::getInstance()->getSelectCamera();
			$oView->tJoinmodel_ComposantWebcam=model_composant::getInstance()->getSelectWebcam();
			$oView->tJoinmodel_ComposantGPS=model_composant::getInstance()->getSelectGPS();
			$oView->tJoinmodel_ComposantClavier=model_composant::getInstance()->getSelectClavier();
			$oView->tJoinmodel_ComposantBoitier=model_composant::getInstance()->getSelectBoitier();
			$oView->tJoinmodel_OS=model_systemeexplotation::getInstance()->getSelect();
			
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new() {
		if ($_SESSION ['suivi'] == - 1) { // si la date correspondat a une date de suivi et n'est pas entre 2 suivis
			$oView = new _view ( 'choixcomposant::nosuivi' );
		} else {
			$tMessage = $this->processSave ();
			
			$oChoixcomposant = new row_choixcomposant ();
			
			$oView = new _view ( 'choixcomposant::new' );
			$oView->oChoixcomposant = $oChoixcomposant;
			$oView->tJoinmodel_ComposantEcran = model_composant::getInstance ()->getSelectEcran ();
			$oView->tJoinmodel_ComposantProcesseur = model_composant::getInstance ()->getSelectProcesseur ();
			$oView->tJoinmodel_ComposantMemoire = model_composant::getInstance ()->getSelectMemoire ();
			$oView->tJoinmodel_ComposantWireless = model_composant::getInstance ()->getSelectWireless ();
			$oView->tJoinmodel_ComposantBattrie = model_composant::getInstance ()->getSelectBatterie ();
			$oView->tJoinmodel_ComposantCamera = model_composant::getInstance ()->getSelectCamera ();
			$oView->tJoinmodel_ComposantWebcam = model_composant::getInstance ()->getSelectWebcam ();
			$oView->tJoinmodel_ComposantGPS = model_composant::getInstance ()->getSelectGPS ();
			$oView->tJoinmodel_ComposantClavier = model_composant::getInstance ()->getSelectClavier ();
			$oView->tJoinmodel_ComposantBoitier = model_composant::getInstance ()->getSelectBoitier ();
			$oView->tJoinmodel_OS = model_systemeexplotation::getInstance ()->getSelect ();
			
			$oPluginXsrf = new plugin_xsrf ();
			$oView->token = $oPluginXsrf->getToken ();
			$oView->tMessage = $tMessage;
		}
		
		$this->oLayout->add ( 'main', $oView );
	}

	
	
	public function _edit(){
		if ($_SESSION ['suivi'] == - 1) { // si la date correspondat a une date de suivi et n'est pas entre 2 suivis
			$oView = new _view ( 'choixcomposant::nosuivi' );
		} else {
		$tMessage=$this->processSave();
		
		
		$oChoixcomposant=model_choixcomposant::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('choixcomposant::edit');
		$oView->oChoixcomposant=$oChoixcomposant;
		$oView->tabId=_root::getParam('id');
		
		$oView->tJoinmodel_ComposantEcran=model_composant::getInstance()->getSelectEcran();
		$oView->tJoinmodel_ComposantProcesseur=model_composant::getInstance()->getSelectProcesseur();
		$oView->tJoinmodel_ComposantMemoire=model_composant::getInstance()->getSelectMemoire();
		$oView->tJoinmodel_ComposantWireless=model_composant::getInstance()->getSelectWireless();
		$oView->tJoinmodel_ComposantBattrie=model_composant::getInstance()->getSelectBatterie();
		$oView->tJoinmodel_ComposantCamera=model_composant::getInstance()->getSelectCamera();
		$oView->tJoinmodel_ComposantWebcam=model_composant::getInstance()->getSelectWebcam();
		$oView->tJoinmodel_ComposantGPS=model_composant::getInstance()->getSelectGPS();
		$oView->tJoinmodel_ComposantClavier=model_composant::getInstance()->getSelectClavier();
		$oView->tJoinmodel_ComposantBoitier=model_composant::getInstance()->getSelectBoitier();
		$oView->tJoinmodel_OS=model_systemeexplotation::getInstance()->getSelect();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		}
		
		$this->oLayout->add('main',$oView);
	}

	public function _detail() {
		if (_root::getParam ( 'id' )){
			$oChoixcomposant = model_choixcomposant::getInstance ()->findById ( _root::getParam ( 'id' ) );
		}elseif (_root::getParam('suivi')){
			$oChoixcomposant = model_choixcomposant::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'],_root::getParam ( 'suivi' ) );
		}
		
		$oView = new _view ( 'choixcomposant::detail' );
		$oView->oChoixcomposant = $oChoixcomposant;
		
		$oView->tJoinmodel_ComposantEcran = model_composant::getInstance ()->getSelectEcran ();
		$oView->tJoinmodel_ComposantProcesseur = model_composant::getInstance ()->getSelectProcesseur ();
		$oView->tJoinmodel_ComposantMemoire = model_composant::getInstance ()->getSelectMemoire ();
		$oView->tJoinmodel_ComposantWireless = model_composant::getInstance ()->getSelectWireless ();
		$oView->tJoinmodel_ComposantBattrie = model_composant::getInstance ()->getSelectBatterie ();
		$oView->tJoinmodel_ComposantCamera = model_composant::getInstance ()->getSelectCamera ();
		$oView->tJoinmodel_ComposantWebcam = model_composant::getInstance ()->getSelectWebcam ();
		$oView->tJoinmodel_ComposantGPS = model_composant::getInstance ()->getSelectGPS ();
		$oView->tJoinmodel_ComposantClavier = model_composant::getInstance ()->getSelectClavier ();
		$oView->tJoinmodel_ComposantBoitier = model_composant::getInstance ()->getSelectBoitier ();
		$oView->tJoinmodel_OS = model_systemeexplotation::getInstance ()->getSelect ();
		
		$this->oLayout->add ( 'main', $oView );
	}
	
	
	

	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oChoixcomposant=new row_choixcomposant;	
		}else{
			$oChoixcomposant=model_choixcomposant::getInstance()->findById( _root::getParam('id',null) );
		}
		$oChoixcomposant->id_groupe=$_SESSION['nEquipe'];
		$oChoixcomposant->id_simulation=$_SESSION['simulation'];
		$oChoixcomposant->id_avancement=$_SESSION['suivi'];
		$tColumn=array('id_ecran','id_processeur','id_memoire','id_wireless','id_battrie','id_camera','id_webcam','id_gps','id_clavier','id_boitier','id_os');
		foreach($tColumn as $sColumn){
			$oChoixcomposant->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oChoixcomposant->save()){
			//une fois enregistre on redirige (vers la page liste)
			//_root::redirect('choixcomposant::list');
		}else{
			return $oChoixcomposant->getListError();
		}
		
	}
	
	private function ProcessValidation() {
		if (! _root::getRequest ()->isPost ()) { // si ce n'est pas une requete POST on ne valide pas
			return null;
		}
		
		$oPluginXsrf = new plugin_xsrf ();
		if (! $oPluginXsrf->checkToken ( _root::getParam ( 'token' ) )) { // on verifie que le token est valide
			return array (
					'token' => $oPluginXsrf->getMessage () 
			);
		}
		
		if (model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
			$oValidation = model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		} else {
			$oValidation = new row_parametrevalidation ();
		}
		
		$oValidation->id_equipe = $_SESSION ['nEquipe'];
		$oValidation->id_simu = $_SESSION ['simulation'];
		$oValidation->id_suivi = $_SESSION ['suivi'];
		$oValidation->tablette_valid = 1;
		
		if ($oValidation->save ()) {
			// calcul des resulats liées au composant de la tablette
			if (model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
				$oResultat = model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
			} else {
				$oResultat = new row_resultat ();
			}
			$oChoixcomposant=model_choixcomposant::getInstance()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'],$_SESSION ['suivi'] );
			
			$qualite_mp = 0;
			$cout_unit_proto = 0;
			$cout_unit_prod = 0;
			$duree_sup_proto = 0;
			$duree_sup_prod = 0;
			$poids = 0;
			$puissance = 0;
			$tColumn = array (
					'id_ecran',
					'id_processeur',
					'id_memoire',
					'id_wireless',
					'id_battrie',
					'id_camera',
					'id_webcam',
					'id_gps',
					'id_clavier',
					'id_boitier' 
			);
			foreach ( $tColumn as $sColumn ) {
				$oComposant = model_composant::getInstance ()->findById ( $oChoixcomposant->$sColumn );
				// calcul qualite mp
				$qualite_mp = $qualite_mp + $oComposant->point_qualite - $oComposant->qualite_hasard; // revoir le sens de quait� hasard
				                                                                                      // calcul cout unitaire matiere premiere production
				$cout_unit_prod = $cout_unit_prod + $oComposant->cout_unit_prod;
				// calcul cout unitaire matiere premiere prototype
				$cout_unit_proto = $cout_unit_proto + $oComposant->cout_unit_proto;
				// duree supplementaire prototype
				$duree_sup_proto = $duree_sup_proto + $oComposant->duree_sup_proto;
				// duree supplementaire production
				$duree_sup_prod = $duree_sup_prod + $oComposant->duree_sup_prod;
				// calcul du poids
				$poids = $poids + $oComposant->poids;
				// calcul de la puissance
				$puissance = $puissance + $oComposant->puissance;
			}
			// qualit� totale matiere premiere
			$oResultat->qualite_mp = $qualite_mp;
			
			// cout total de matiere premiere
			$oResultat->cout_mp = $cout_unit_proto + 2000 * $cout_unit_prod;
			$oResultat->duree_sup_proto = $duree_sup_proto;
			$oResultat->duree_sup_prod = $duree_sup_prod;
			$oResultat->poids = $poids;
			$oResultat->puissance = $puissance;
			$oResultat->id_simu = $_SESSION ['simulation'];
			$oResultat->id_suivi = $_SESSION ['suivi'];
			$oResultat->id_equipe = $_SESSION ['nEquipe'];
			
			if ($oResultat->save ()) {
					_root::redirect('choixcomposant::detail',array('id'=>$oChoixcomposant->id));
			} else {
				return $oResultat->getListError ();
			}
		} else {
			return $oValidation->getListError ();
		}
	}

	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

