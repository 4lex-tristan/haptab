<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">HapTab</a>
		</div>
		<div class="collapse navbar-collapse">

			<ul class="nav navbar-nav">
			<?php foreach($this->tLink as $sLibelle => $sLink): ?>
			<?php if(!is_array($sLink)):?>
				<?php if(_root::getParamNav()==$sLink):?>
					<li class="active nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLink) ?>"><?php echo $sLibelle ?></a></li>
				<?php else:?>
					<li class="nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLink) ?>"><?php echo $sLibelle ?></a></li>
				<?php endif;?>
			<?php else:?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $sLibelle ?><span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
					<?php $tLinkDrop = $sLink; // sLink etant un array, nous le parsons
					foreach($tLinkDrop as $sLibelleDrop => $sLinkDrop): ?>
						<?php if(_root::getParamNav()==$sLink):?>
							<li class="active nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLinkDrop) ?>"><?php echo $sLibelleDrop ?></a></li>
						<?php else:?>
							<li class="nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLinkDrop) ?>"><?php echo $sLibelleDrop ?></a></li>
						<?php endif;?>
					<?php endforeach;?>
					</ul>
				</li>
			<?php endif;?>
			<?php endforeach;?>
			</ul>
			<form class="navbar-form navbar-right"> 
			<?php foreach($this->tLink2 as $sLibelle2 => $sLink2): ?>
				<button href="<?php echo $this->getLink($sLink2) ?>" class="btn btn-secondary" type="submit" ><?php echo $sLibelle2 ?></button>
				<?php endforeach;?>
			</form>				
		</div><!--/.nav-collapse -->
	</div>
</nav>
