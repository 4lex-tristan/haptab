<?php
class module_menuAdminAll extends abstract_moduleembedded {
	public function _index() {
		/*$tLink = array (
				'Comptes' => 'account::list',
				'Composants' => 'composant::list',
				'Pays' => 'contraintepays::list',
				'Groupes' => 'groupe::list',
				'Droits' => 'Permission::list',
				'R.H.' => 'ressourceshumaines::list',
				'Simulation' => 'simulation::list',
				'O.S.' => 'systemeexplotation::list',
				'Taches' => 'taches::list',
				'Var. qualit&eacute; comp.' => 'variationsqualite::list',
				'Var. R.H.' => 'variationsressources::list' 
		);*/
		$tLink = array (
				'TdB' =>'tdbAdmin::Index',
				'Gestion des droits' => array(
						'Comptes' => 'account::list',
						'Groupes' => 'groupe::list',
						'Droits' => 'Permission::list'),
				'Simu. & Pénalités' => array(
						'Simualtion' => 'simulation::list',
						'Pénalités' => 'penalite::index'),
				'Res. materiels'=>array(
						'Composants' => 'composant::list',
						 'O.S.' => 'systemeexplotation::list',
						'Var. qualit&eacute; comp.' => 'variationsqualite::list'),
				'Res. Humaines'=>array(
						'Pays' => 'contraintepays::list',
						'R.H.' => 'ressourceshumaines::list',
						'Var. R.H.' => 'variationsressources::list'
				),
				'Taches' => 'taches::list',
				'Risques et évènements'=>array(
						'Risques'=>'risque::index',
						'Mitigations'=>'mitigationrisque::index',
						'Evenements' => 'evenements::index',
						'Mesures Correct.' => 'correctionsevent::index'
				)
		);
		$tLink2 = array (
				'Log out' => 'auth::logout' 
		);
		
		$oView = new _view ( 'menuAdminAll::index' );
		$oView->tLink = $tLink;
		$oView->tLink2 = $tLink2;
		
		return $oView;
	}
}
