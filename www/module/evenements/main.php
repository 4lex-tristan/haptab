<?php 
class module_evenements extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tEvents=model_evenements::getInstance()->findAll();
		
		$oView=new _view('evenements::list');
		$oView->tEvents=$tEvents;
		
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->tJoinmodel_suivi=model_datessuivi::getInstance()->getSuivi();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oEvent=new row_evenements;
		
		$oView=new _view('evenements::new');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->tJoinmodel_suivi=model_datessuivi::getInstance()->getSuivi();
		$oView->oEvent=$oEvent;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oEvent=model_evenements::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('evenements::edit');
		$oView->oEvent=$oEvent;
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->tJoinmodel_suivi=model_datessuivi::getInstance()->getSuivi();	
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _show(){
		$oEvent=model_evenements::getInstance()->findById( _root::getParam('id') );
		
		
		$oView=new _view('evenements::show');
		$oView->oEvent=$oEvent;
		
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oEvent=model_evenements::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('evenements::delete');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->oEvent=$oEvent;
		$oView->tJoinmodel_suivi=model_datessuivi::getInstance()->getSuivi();
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oRisque=new row_evenements;	
		}else{
			$oRisque=model_evenements::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('nom','description','id_simu','id_suivi');
		foreach($tColumn as $sColumn){
			$oRisque->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oRisque->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('evenements::list');
		}else{
			return $oRisque->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oRisque=model_evenements::getInstance()->findById( _root::getParam('id',null) );
				
		$oRisque->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('evenements::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

