<?php 
$oForm=new plugin_form($this->oEvent);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de l'évènement</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description de l'évènement</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('description',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_simu',$this->tJoinmodel_simulation,array('class'=>'form-control'));?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_suivi',$this->tJoinmodel_suivi,array('class'=>'form-control'));?></div>
	</div>

<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Ajouter" /> <a class="btn btn-link" href="<?php echo $this->getLink('evenements::list')?>">Annuler</a>
	</div>
</div>
</form>
