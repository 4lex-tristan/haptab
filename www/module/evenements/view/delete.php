<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de l'évènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description de l'évènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->description ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oEvent->id_simu] ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Suivi</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_suivi[$this->oEvent->id_suivi] ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('evenements::list')?>">Annuler</a>
	</div>
</div>
</form>
