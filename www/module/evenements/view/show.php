<form class="form-horizontal" action="" method="POST" >
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de l'évènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description de l'évènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->description ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Mesures correctives</label>
		<div class="col-sm-10">
			<div class="table-responsive">
			<table class="table">
			<thead>
			<th>Intitulé</th>
			<th>Qualité</th>
			<th>Cout</th>
			<th>Délai</th>
			</thead>
			<?php $tMitigations=model_mitigationrisque::getInstance()->findAllByRisque( $this->oEvent->id );
		foreach ($tMitigations as $oMitigation):?>
		<tr>
		<td><?php echo $oMitigation->niveau?></td>
		<td><a class="btn btn-link" href="<?php echo $this->getLink('mitigationrisque::show&id='.$oMitigation->id_mitigation)?>"><?php echo $oMitigation->intitule?></a></td>
		</tr>
		<?php endforeach;?>
		</table>
		</div>
		</div>
	</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		 <a class="btn btn-default" href="<?php echo $this->getLink('evenements::list')?>">Retour</a>
	</div>
</div>
</form>
