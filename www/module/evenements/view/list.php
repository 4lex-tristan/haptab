<table class="table table-striped">
	<tr>
	
		<th>Simulation</th>
	
		<th>Suivi</th>
		
		<th>Nom de l'évènement</th>

		<th>Description de l'évènement</th>

		<th></th>
	</tr>
	<?php if($this->tEvents):?>
		<?php foreach($this->tEvents as $oEvent):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
		<td><?php echo $this->tJoinmodel_simulation[$oEvent->id_simu] ?></td>
		<td><?php if(isset($this->tJoinmodel_suivi[$oEvent->id_suivi])) {echo $this->tJoinmodel_suivi[$oEvent->id_suivi]; }?></td>
		<td><?php echo $oEvent->nom ?></td>

		<td><?php echo $oEvent->description ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('evenements::edit',array(
										'id'=>$oEvent->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('evenements::delete',array(
										'id'=>$oEvent->getId()
									) 
							)?>">Delete</a>

<a class="btn btn-default" href="<?php echo $this->getLink('evenements::show',array(
										'id'=>$oEvent->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="3">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('evenements::new') ?>">New</a></p>

