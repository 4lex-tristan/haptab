<?php 
$oForm=new plugin_form($this->oPenalite);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_simu',$this->tJoinmodel_simulation,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un budget d&eacute;pass&eacute; (p&eacute;nalit&eacute; /$)</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('budget',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un d&eacute;lai d&eacute;pass&eacute; (penalit&eacute;/$</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('delai',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour une qualit&eacute; insuffisant (p&eacute;nalit&eacute;/point)</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un poids non conforme</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('poids',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour une puissance non conforme</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('battrie',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Ajouter" /> <a class="btn btn-link" href="<?php echo $this->getLink('penalite::list')?>">Annuler</a>
	</div>
</div>
</form>
