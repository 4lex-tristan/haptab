<table class="table table-striped">
	<tr>
		
		<th>Simulation</th>

		<th>P&eacute;nalit&eacute; pour un budget d&eacute;pass&eacute; (p&eacute;nalit&eacute; /$)</th>

		<th>P&eacute;nalit&eacute; pour un d&eacute;lai d&eacute;pass&eacute; (penalit&eacute;/$</th>

		<th>P&eacute;nalit&eacute; pour une qualit&eacute; insuffisant (p&eacute;nalit&eacute;/point)</th>

		<th>P&eacute;nalit&eacute; pour un poids non conforme</th>

		<th>P&eacute;nalit&eacute; pour une puissance non conforme</th>

		<th></th>
	</tr>
	<?php if($this->tPenalite):?>
		<?php foreach($this->tPenalite as $oPenalite):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $this->tJoinmodel_simulation[$oPenalite->id_simu] ?></td>

		<td><?php echo $oPenalite->budget ?></td>

		<td><?php echo $oPenalite->delai ?></td>

		<td><?php echo $oPenalite->qualite ?></td>

		<td><?php echo $oPenalite->poids ?></td>

		<td><?php echo $oPenalite->battrie ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('penalite::edit',array(
										'id'=>$oPenalite->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('penalite::delete',array(
										'id'=>$oPenalite->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="7">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('penalite::new') ?>">New</a></p>

