<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oPenalite->id_simu] ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un budget d&eacute;pass&eacute; (p&eacute;nalit&eacute; /$)</label>
		<div class="col-sm-10"><?php echo $this->oPenalite->budget ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un d&eacute;lai d&eacute;pass&eacute; (penalit&eacute;/$</label>
		<div class="col-sm-10"><?php echo $this->oPenalite->delai ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour une qualit&eacute; insuffisant (p&eacute;nalit&eacute;/point)</label>
		<div class="col-sm-10"><?php echo $this->oPenalite->qualite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour un poids non conforme</label>
		<div class="col-sm-10"><?php echo $this->oPenalite->poids ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">P&eacute;nalit&eacute; pour une puissance non conforme</label>
		<div class="col-sm-10"><?php echo $this->oPenalite->battrie ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('penalite::list')?>">Annuler</a>
	</div>
</div>
</form>
