<?php 
class module_penalite extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		$this->oLayout=new _layout('bootstrap');
		
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tPenalite=model_penalite::getInstance()->findAll();
		
		$oView=new _view('penalite::list');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->tPenalite=$tPenalite;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oPenalite=new row_penalite;
		
		$oView=new _view('penalite::new');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->oPenalite=$oPenalite;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oPenalite=model_penalite::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('penalite::edit');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->oPenalite=$oPenalite;
		$oView->tId=model_penalite::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oPenalite=model_penalite::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('penalite::delete');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->oPenalite=$oPenalite;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oPenalite=new row_penalite;	
		}else{
			$oPenalite=model_penalite::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('id_simu','budget','delai','qualite','poids','battrie');
		foreach($tColumn as $sColumn){
			$oPenalite->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oPenalite->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('penalite::list');
		}else{
			return $oPenalite->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oPenalite=model_penalite::getInstance()->findById( _root::getParam('id',null) );
				
		$oPenalite->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('penalite::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

