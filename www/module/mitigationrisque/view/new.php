<?php 
$oForm=new plugin_form($this->oMitigationrisque);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Risque</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_risque',$this->tJoinmodel_risque,array('class'=>'form-control'));?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Niveau de mitigation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('niveau',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Intitul�</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('intitule',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('description',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur le d&eacute;lai en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('impact_delai',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur le co&ucirc;t en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('impact_cout',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur la qualit&eacute; en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('impact_qualite',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur le delai</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('delai',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur le cout</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('cout',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur la qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Ajouter" /> <a class="btn btn-link" href="<?php echo $this->getLink('mitigationrisque::list')?>">Annuler</a>
	</div>
</div>
</form>
