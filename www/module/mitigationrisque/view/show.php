<form class="form-horizontal" action="" method="POST" >
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Risque</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_risque[$this->oMitigationrisque->id_risque]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Niveau de mitigation</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->niveau ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Intitul�</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('intitule',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('description',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur le d&eacute;lai en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->impact_delai ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur le co&ucirc;t en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->impact_cout ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur la qualit&eacute; en cas d&#039;occurence</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->impact_qualite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur le cout</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->cout ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur la qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->qualite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Contre parti sur le delai</label>
		<div class="col-sm-10"><?php echo $this->oMitigationrisque->delai ?></div>
	</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		 <a class="btn btn-warning" href="<?php echo $this->getLink('mitigationrisque::edit&id='.$this->oMitigationrisque->id_mitigation)?>">Modifier</a> <a class="btn btn-default" href="<?php echo $this->getLink('mitigationrisque::list')?>">Retour</a>
	</div>
</div>
</form>
