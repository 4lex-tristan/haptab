<table class="table table-striped">
	<tr>
		
		<th>Risque</th>

		<th>Niveau de mitigation</th>
		
		<th>Intitul�</th>

		<th>Impact sur le d&eacute;lai en cas d&#039;occurence</th>

		<th>Impact sur le co&ucirc;t en cas d&#039;occurence</th>

		<th>Impact sur la qualit&eacute; en cas d&#039;occurence</th>

		<th>Contre parti sur le cout</th>

		<th>Contre parti sur la qualit&eacute;</th>

		<th>Contre parti sur le delai</th>

		<th></th>
	</tr>
	<?php if($this->tMitigationrisque):?>
		<?php foreach($this->tMitigationrisque as $oMitigationrisque):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php if(isset($this->tJoinmodel_risque[$oMitigationrisque->id_risque])){ echo $this->tJoinmodel_risque[$oMitigationrisque->id_risque];}else{ echo $oMitigationrisque->id_risque ;}?></td>

		<td><?php echo $oMitigationrisque->niveau ?></td>
		
		<td><?php echo $oMitigationrisque->intitule ?></td>

		<td><?php echo $oMitigationrisque->impact_delai ?></td>

		<td><?php echo $oMitigationrisque->impact_cout ?></td>

		<td><?php echo $oMitigationrisque->impact_qualite ?></td>

		<td><?php echo $oMitigationrisque->cout ?></td>

		<td><?php echo $oMitigationrisque->qualite ?></td>

		<td><?php echo $oMitigationrisque->delai ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('mitigationrisque::edit',array(
										'id'=>$oMitigationrisque->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('mitigationrisque::delete',array(
										'id'=>$oMitigationrisque->getId()
									) 
							)?>">Delete</a>

<a class="btn btn-default" href="<?php echo $this->getLink('mitigationrisque::show',array(
										'id'=>$oMitigationrisque->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="9">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('mitigationrisque::new') ?>">New</a></p>

