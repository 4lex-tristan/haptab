<?php 
class module_mitigationrisque extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');		
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tMitigationrisque=model_mitigationrisque::getInstance()->findAll();
		
		$oView=new _view('mitigationrisque::list');
		$oView->tMitigationrisque=$tMitigationrisque;
		
				$oView->tJoinmodel_risque=model_risque::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oMitigationrisque=new row_mitigationrisque;
		
		$oView=new _view('mitigationrisque::new');
		$oView->oMitigationrisque=$oMitigationrisque;
		
				$oView->tJoinmodel_risque=model_risque::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oMitigationrisque=model_mitigationrisque::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('mitigationrisque::edit');
		$oView->oMitigationrisque=$oMitigationrisque;
		$oView->tId=model_mitigationrisque::getInstance()->getIdTab();
		
				$oView->tJoinmodel_risque=model_risque::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _show(){
		$oMitigationrisque=model_mitigationrisque::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('mitigationrisque::show');
		$oView->oMitigationrisque=$oMitigationrisque;
		
				$oView->tJoinmodel_risque=model_risque::getInstance()->getSelect();
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oMitigationrisque=model_mitigationrisque::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('mitigationrisque::delete');
		$oView->oMitigationrisque=$oMitigationrisque;
		
				$oView->tJoinmodel_risque=model_risque::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oMitigationrisque=new row_mitigationrisque;	
		}else{
			$oMitigationrisque=model_mitigationrisque::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('id_risque','niveau','intitule','description','impact_delai','impact_cout','impact_qualite','cout','qualite','delai');
		foreach($tColumn as $sColumn){
			$oMitigationrisque->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oMitigationrisque->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('mitigationrisque::list');
		}else{
			return $oMitigationrisque->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oMitigationrisque=model_mitigationrisque::getInstance()->findById( _root::getParam('id',null) );
				
		$oMitigationrisque->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('mitigationrisque::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

