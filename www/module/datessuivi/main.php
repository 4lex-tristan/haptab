<?php 
class module_datessuivi extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');		
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
		
	
	public function _new(){
		
		$tMessage=$this->processSave();
	
		$oDatessuivi=new row_datessuivi;
		
		$oView=new _view('datessuivi::new');
		$oView->oDatessuivi=$oDatessuivi;
		$oView->idSimu=_root::getParam('idsimu');
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oDatessuivi=model_datessuivi::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('datessuivi::edit');
		$oView->oDatessuivi=$oDatessuivi;
		$oView->tId=model_datessuivi::getInstance()->getIdTab(); // to delete never called
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oDatessuivi=model_datessuivi::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('datessuivi::delete');
		$oView->oDatessuivi=$oDatessuivi;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oDatessuivi=new row_datessuivi;	
		}else{
			$oDatessuivi=model_datessuivi::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('IdSimulation', 'dateDebut','dateFin', 'timeDebut', 'timeFin');
		foreach($tColumn as $sColumn){
			$oDatessuivi->$sColumn=_root::getParam($sColumn,null) ;
		}

		$oDatessuivi->timestampDebut = $this->generate_timestamp_from_date_and_time($oDatessuivi->dateDebut, $oDatessuivi->timeDebut);
		$oDatessuivi->timestampFin = $this->generate_timestamp_from_date_and_time($oDatessuivi->dateFin, $oDatessuivi->timeFin);
		
		if($oDatessuivi->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('simulation::show', array('id'=>_root::getParam('IdSimulation')));
		}else{
			return $oDatessuivi->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oDatessuivi=model_datessuivi::getInstance()->findById( _root::getParam('id',null) );
		$idSimu = $oDatessuivi->IdSimulation;
				
		$oDatessuivi->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('simulation::show', array('id'=>$idSimu));
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	private function generate_timestamp_from_date_and_time($date,$time){
		// convertir les date et heure de debut en timestamp
		date_default_timezone_set('UTC');
		//$d = new DateTime('2016-03-11 11:00:00')
		$d = new DateTime($date.' '.$time);
		return $d->getTimestamp();
	}
	
}

