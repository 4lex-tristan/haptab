<form class="form-horizontal" action="" method="POST">

	<div class="form-group">
		<label class="col-sm-2 control-label">Date de d&eacute;but de suivi</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date($this->oDatessuivi->dateDebut); echo $oDate->toString('d-m-Y'); ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heure de d&eacute;but de suivi</label>
		<div class="col-sm-10"><?php echo $this->oDatessuivi->timeDebut; ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Date de fin de suivi</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date($this->oDatessuivi->dateFin); echo $oDate->toString('d-m-Y'); ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heure de fin de suivi</label>
		<div class="col-sm-10"><?php echo $this->oDatessuivi->timeFin; ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('simulation::show', array('id'=>$this->oDatessuivi->IdSimulation))?>">Annuler</a>
	</div>
</div>
</form>
