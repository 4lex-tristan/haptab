<?php 
$oForm=new plugin_form($this->oDatessuivi);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	<?php echo $oForm->getInputHidden('IdSimulation',array('class'=>'form-control'))?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Date de d&eacute;but de suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputDate('dateDebut',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heure de d&eacute;but de suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputTime('timeDebut',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Date de fin de suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputDate('dateFin',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heure de fin de suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputTime('timeFin',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('simulation::show', array('id'=>$this->oDatessuivi->IdSimulation))?>">Annuler</a>
	</div>
</div>
</form>

