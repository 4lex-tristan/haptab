<?php 
class module_tdbGroupe extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuSimulation::index');
		//$this->oLayout->addModule('menu','menu::index');
	}
	/* #debutaction#
	public function _exampleaction(){
	
		$oView=new _view('examplemodule::exampleaction');
		
		$this->oLayout->add('main',$oView);
	}
	#finaction# */
	
	public function _index() {
		$oView=new _view('tdbGroupe::index');
		$tSuivis=model_datessuivi::getInstance()->findBySimu($_SESSION['simulation']);
		foreach ($tSuivis as $oSuivi){
			$oEvenement = model_evenements::getInstance()->findByContext($_SESSION['simulation'],$oSuivi->id);
			(isset($oEvenement) && $oEvenement != NULL) ? $tHasEvenements[] = 1 : $tHasEvenements[] = 0 ;
			$oResultat=model_resultat::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id);
			$tResultats[]=$oResultat;
			$oValidation=model_parametrevalidation::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id);
			$tValidations[]=$oValidation;
			if(model_choixcorrections::getInstance()->isFulfilled($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id)){
				$bEvent=1;
			}else{
				$bEvent=0;
			}
			if(model_choixcomposant::getInstance()->isFulfilled($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id)){
				$bTablette=1;
			}else{
				$bTablette=0;
			}
			if (model_tachessimulees::getInstance()->isFulfilled($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id)){
				$bTache=1;
			}else{
				$bTache=0;
			}
			if (model_choixmitigation::getInstance()->isFulfilled($_SESSION['nEquipe'],$_SESSION['simulation'],$oSuivi->id)){
				$bRisque=1;
			}else{
				$bRisque=0;
			}
			
			$tFulfilled[]=array('event'=>$bEvent,'tablette'=>$bTablette,'tache'=>$bTache,'risque'=>$bRisque);
		}
		$oView->tSuivis=$tSuivis;
		if (sizeof($tSuivis) ==0){
			$oView->tResultats=array(0);	
			$oView->tValidations=array(0);
			$oView->tFulfilled=array('event'=>0,'tablette'=>0,'tache'=>0,'risque'=>0);
		}else{
			$oView->tResultats=$tResultats;	
			$oView->tValidations=$tValidations;
			$oView->tHasEvenements=$tHasEvenements;
			$oView->tFulfilled=$tFulfilled;
		}
		
		
		$this->oLayout->add('main',$oView);
	}
	
	public function after(){
		$this->oLayout->show();
	}
	
	
}
