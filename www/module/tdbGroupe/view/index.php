<div class="table-responsive">
<table class="table">
<thead>
<th>Numéro d'identification</th>
<th>Nom d'équipe</th>
<!--<th>Suivi actuel</th>-->
</thead>
<tr>
<td><?php echo  $_SESSION ['nEquipe'];?></td>
<td><?php $oAccount= model_account::getInstance()->findById( $_SESSION ['nEquipe']); echo $oAccount->login?></td>
<!--<td><?php //echo $oAccount->num_suivi?></td>-->
</tr>
</table>
</div>
<h2 class="sub-header">Progression</h2>
<div class="table-responsive">
	<table class="table">
		<thead>
			<th>Suivi</th>
			<th>Date</th>
			<th>Evènement</th>
			<th>Risque</th>
			<th>Tablette</th>
			<th>Taches</th>
			
		</thead>
		<?php for($i=0;$i<sizeof($this->tSuivis);$i++): ?>
		<tr>
			<td><?php echo $i;?></td>
			<td><?php $oDateDebut= new plugin_date($this->tSuivis[$i]->dateDebut); $oDateFin= new plugin_date($this->tSuivis[$i]->dateFin); echo 'du '.$oDateDebut->toString('d-m-Y').' '.$this->tSuivis[$i]->timeDebut.' au '.$oDateFin->toString('d-m-Y').' '.$this->tSuivis[$i]->timeFin ;  ?></td>
			<?php 
			if (time() > $this->tSuivis[$i]->timestampDebut):?>
			<td><?php 
			if($this->tHasEvenements[$i]):
			if(isset($this->tValidations[$i]->event_valid) && $this->tValidations[$i]->event_valid == 1):?> 
				<p>
				<!-- detail -->
					<a class="btn btn-default" href="<?php echo $this->getLink('choixcorrection::detail',array('suivi'=>$this->tSuivis[$i]->id)) ?>">Détails</a>
				</p>
			<?php elseif ($this->tFulfilled[$i]['event'] == 1):?>
				<p>
				<!-- validation -->
					<a class="btn btn-warning" href="<?php echo $this->getLink('choixcorrection::edit') ?>">A valider</a>
				</p>
			<?php else:?>
				<p>
					<a class="btn btn-danger" href="<?php echo $this->getLink('choixcorrection::new') ?>">A completer</a>
				</p>
			<?php endif;
			endif;?>
			</td>
			<td> <?php if(isset($this->tValidations[$i]->risque_valid) && $this->tValidations[$i]->risque_valid == 1):?>
				<p>
					<a class="btn btn-default" href="<?php echo $this->getLink('choixmitigation::detail',array('suivi'=>$this->tSuivis[$i]->id)) ?>">Détails</a>
				</p>
			<?php elseif ($this->tFulfilled[$i]['risque'] == 1):?>
				<p>
					<a class="btn btn-warning" href="<?php echo $this->getLink('choixmitigation::editlist') ?>">A valider</a>
				</p>
			<?php else:?>
				<p>
					<a class="btn btn-danger" href="<?php echo $this->getLink('choixmitigation::new') ?>">A completer</a>
				</p>
			<?php endif;?>
			</td>
			<td><?php if(isset($this->tValidations[$i]->tablette_valid) && $this->tValidations[$i]->tablette_valid == 1):?> 
				<p>
				<!-- detail -->
					<a class="btn btn-default" href="<?php echo $this->getLink('choixcomposant::detail',array('suivi'=>$this->tSuivis[$i]->id)) ?>">Détails</a>
				</p>
			<?php elseif ($this->tFulfilled[$i]['tablette'] == 1):?>
				<p>
				<!-- validation -->
					<a class="btn btn-warning" href="<?php echo $this->getLink('choixcomposant::validate',array('suivi'=>$this->tSuivis[$i]->id)) ?>">A valider</a>
				</p>
			<?php else:?>
				<p>
					<a class="btn btn-danger" href="<?php echo $this->getLink('choixcomposant::new') ?>">A completer</a>
				</p>
			<?php endif;?>
			</td>
			<td><?php if(isset($this->tValidations[$i]->tache_valid) && $this->tValidations[$i]->tache_valid == 1):?>
				<p>
					<a class="btn btn-default" href="<?php echo $this->getLink('tachessimulees::detail',array('suivi'=>$this->tSuivis[$i]->idSuivi)) ?>">Détails</a>
				</p>
			<?php elseif ($this->tFulfilled[$i]['tache'] == 1):?>
				<p>
					<a class="btn btn-warning" href="<?php echo $this->getLink('tachessimulees::editlist',array('suivi'=>$_SESSION['suivi'])) ?>">A valider</a>
				</p>
			<?php else:?>
				<p>
					<a class="btn btn-danger" href="<?php echo $this->getLink('tachessimulees::new') ?>">A completer</a>
				</p>
			<?php endif;?>
			</td>
			<?php endif;?>
		</tr>
		<?php endfor;?>
	</table>
</div>

<h2 class="sub-header">Résultats</h2>
<h3 class="sub-header">Généraux</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
			<th></th>
			<th>Qualité totale</th>
			<th>Cout total</th>
			<th>Durée</th>
			<th>Score</th>
		</thead>

			<?php for($i=0;$i<sizeof($this->tSuivis);$i++):?>
			<tr>
			<?php if(isset($this->tResultats[$i])):?>
			<td><?php echo 'Suivi '.$i;?></td>
			<?php //if ($oResultat=model_resultat::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$this->tSuivis[$i]->id)):?>
			
			<td><?php echo $this->tResultats[$i]->qualite_tot?></td>
			<td><?php echo $this->tResultats[$i]->cout_tot?></td>
			<td><?php echo $this->tResultats[$i]->delai_tot?></td>
			<td></td>
			<?php endif;?>
			</tr>
		
			<?php endfor;?>
	</table>
</div>

<h3 class="sub-header">Spécifications techniques</h3>
<?php //if($_SESSION['suivi'] == 0 ): ?>

<div class="table-responsive">
	<table class="table">
		<thead>
			<th></th>
			<th>Qualité des composants </th>
			<th>Poids</th>
			<th>Puissance</th>
		</thead>
			<?php for($i=0;$i<sizeof($this->tSuivis);$i++):?>
			<tr>
				<?php if(isset($this->tResultats[$i])):?>
				<td><?php echo 'Suivi '.$i;?></td>
				<td><?php echo $this->tResultats[$i]->qualite_mp;?></td>
				<td><?php echo $this->tResultats[$i]->poids;?></td>
				<td><?php echo $this->tResultats[$i]->puissance;?></td>
				<?php endif;?>
			</tr>
			<?php endfor;?>
	</table>
</div>

<h3 class="sub-header">Détails</h3>
<div class="table-responsive">
	<table class="table">
		<thead>
			<th></th>
			<th>Cout MP</th>
			<th>Cout MO</th>
			<th>Duree additionnel prototype</th>
			<th>Duree additionnel production</th>
		</thead>
			<?php for($i=0;$i<sizeof($this->tSuivis);$i++):?>
			<tr>
				<?php if(isset($this->tResultats[$i])):?>
				<td><?php echo 'Suivi '.$i;?></td>
				<td><?php echo $this->tResultats[$i]->cout_mp;?></td>
				<td><?php echo $this->tResultats[$i]->cout_mo?></td>
				<td><?php echo $this->tResultats[$i]->duree_sup_proto;?></td>
				<td><?php echo $this->tResultats[$i]->duree_sup_prod;?></td>
				<?php endif;?>
			</tr>
			<?php endfor;?>
	</table>
</div>