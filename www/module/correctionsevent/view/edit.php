<?php 
$oForm=new plugin_form($this->oCorrectionsevent);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	<div class="form-group">
		<label class="col-sm-2 control-label">Risque</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_event',$this->tJoinmodel_event,array('class'=>'form-control'));?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Intitul&eacute; de la correction</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur la qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur les co&ucirc;ts</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('cout',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur les d&eacute;lais</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('delai',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('correctionsevent::list')?>">Annuler</a>
	</div>
</div>
</form>

