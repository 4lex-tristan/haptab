<table class="table table-striped">
	<tr>
		<th>Evènement</th>
		
		<th>Intitul&eacute; de la correction</th>

		<th>Impact sur la qualit&eacute;</th>

		<th>Impact sur les co&ucirc;ts</th>

		<th>Impact sur les d&eacute;lais</th>

		<th></th>
	</tr>
	<?php if($this->tCorrectionsevent):?>
		<?php foreach($this->tCorrectionsevent as $oCorrectionsevent):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
		
		<td><?php if (isset($this->tJoinmodel_event[$oCorrectionsevent->id_event])){echo $this->tJoinmodel_event[$oCorrectionsevent->id_event];} else{echo $oCorrectionsevent->id_event;} ?></td>
			
		<td><?php echo $oCorrectionsevent->nom ?></td>

		<td><?php echo $oCorrectionsevent->qualite ?></td>

		<td><?php echo $oCorrectionsevent->cout ?></td>

		<td><?php echo $oCorrectionsevent->delai ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('correctionsevent::edit',array(
										'id'=>$oCorrectionsevent->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('correctionsevent::delete',array(
										'id'=>$oCorrectionsevent->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="5">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('correctionsevent::new') ?>">New</a></p>

