<form class="form-horizontal" action="" method="POST">

	<div class="form-group">
		<label class="col-sm-2 control-label">Evènement</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_event[$this->oCorrectionsevent->id_event]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Intitul&eacute; de la correction</label>
		<div class="col-sm-10"><?php echo $this->oCorrectionsevent->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur la qualit&eacute;</label>
		<div class="col-sm-10"><?php echo $this->oCorrectionsevent->qualite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur les co&ucirc;ts</label>
		<div class="col-sm-10"><?php echo $this->oCorrectionsevent->cout ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Impact sur les d&eacute;lais</label>
		<div class="col-sm-10"><?php echo $this->oCorrectionsevent->delai ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('correctionsevent::list')?>">Annuler</a>
	</div>
</div>
</form>
