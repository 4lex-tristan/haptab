<?php 
class module_correctionsevent extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tCorrectionsevent=model_correctionsevent::getInstance()->findAll();
		
		$oView=new _view('correctionsevent::list');
		$oView->tJoinmodel_event=model_evenements::getInstance()->getSelect();
		$oView->tCorrectionsevent=$tCorrectionsevent;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oCorrectionsevent=new row_correctionsevent;
		
		$oView=new _view('correctionsevent::new');
		$oView->tJoinmodel_event=model_evenements::getInstance()->getSelect();
		$oView->oCorrectionsevent=$oCorrectionsevent;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oCorrectionsevent=model_correctionsevent::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('correctionsevent::edit');
		$oView->tJoinmodel_event=model_evenements::getInstance()->getSelect();
		$oView->oCorrectionsevent=$oCorrectionsevent;
		$oView->tId=model_correctionsevent::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oCorrectionsevent=model_correctionsevent::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('correctionsevent::delete');
		$oView->tJoinmodel_event=model_evenements::getInstance()->getSelect();
		$oView->oCorrectionsevent=$oCorrectionsevent;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oCorrectionsevent=new row_correctionsevent;	
		}else{
			$oCorrectionsevent=model_correctionsevent::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('id_event','nom','qualite','cout','delai');
		foreach($tColumn as $sColumn){
			$oCorrectionsevent->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oCorrectionsevent->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('correctionsevent::list');
		}else{
			return $oCorrectionsevent->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oCorrectionsevent=model_correctionsevent::getInstance()->findById( _root::getParam('id',null) );
				
		$oCorrectionsevent->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('correctionsevent::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

