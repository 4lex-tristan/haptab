<table class="table table-striped">
	<tr>
		
		<th>login</th>

		<th>password</th>

		<th>Simulation</th>

		<th>Groupe de droit</th>

		<th></th>
	</tr>
	<?php if($this->tAccount):?>
		<?php foreach($this->tAccount as $oAccount):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oAccount->login ?></td>

		<td><?php echo $oAccount->password ?></td>

		<td><?php if(isset($this->tJoinmodel_simulation[$oAccount->id_simulation])){ echo $this->tJoinmodel_simulation[$oAccount->id_simulation];}else{ echo $oAccount->id_simulation ;}?></td>

		<td><?php if(isset($this->tJoinmodel_groupe[$oAccount->groupe_id])){ echo $this->tJoinmodel_groupe[$oAccount->groupe_id];}else{ echo $oAccount->groupe_id ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('account::edit',array(
										'id'=>$oAccount->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('account::delete',array(
										'id'=>$oAccount->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="5">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('account::new') ?>">New</a></p>

