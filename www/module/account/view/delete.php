<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">login</label>
		<div class="col-sm-10"><?php echo $this->oAccount->login ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">password</label>
		<div class="col-sm-10"><?php echo $this->oAccount->password ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oAccount->id_simulation]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Groupe de droit</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_groupe[$this->oAccount->groupe_id]?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('account::list')?>">Annuler</a>
	</div>
</div>
</form>
