<?php 
class module_account extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
		//$this->oLayout->addModule('menu','menu::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tAccount=model_account::getInstance()->findAll();
		
		$oView=new _view('account::list');
		$oView->tAccount=$tAccount;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();		$oView->tJoinmodel_groupe=model_groupe::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oAccount=new row_account;
		
		$oView=new _view('account::new');
		$oView->oAccount=$oAccount;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();		$oView->tJoinmodel_groupe=model_groupe::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oAccount=model_account::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('account::edit');
		$oView->oAccount=$oAccount;
		$oView->tId=model_account::getInstance()->getIdTab();
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();		$oView->tJoinmodel_groupe=model_groupe::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oAccount=model_account::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('account::delete');
		$oView->oAccount=$oAccount;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();		$oView->tJoinmodel_groupe=model_groupe::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oAccount=new row_account;	
		}else{
			$oAccount=model_account::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('login','password','id_simulation','groupe_id');
		foreach($tColumn as $sColumn){
			$oAccount->$sColumn=_root::getParam($sColumn,null) ;
			if ($sColumn ==  'password'){
				$oAccount->$sColumn=model_account::getInstance()->hashPassword(_root::getParam($sColumn,null)) ;
			}

		}
		
		
		if($oAccount->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('account::list');
		}else{
			return $oAccount->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oAccount=model_account::getInstance()->findById( _root::getParam('id',null) );
				
		$oAccount->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('account::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

