<form class="form-horizontal" action="" method="POST">

	<div class="form-group">
		<label class="col-sm-2 control-label">Nom</label>
		<div class="col-sm-10"><?php echo $this->oSimulation->name ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nombre de suivis</label>
		<div class="col-sm-10"><?php echo $this->nbSuivi ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Debut de la simulation</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date( $this->oSimulation->dateDebut); echo $oDate->toString('d-m-Y'); ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Fin de la simulation</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date($this->oSimulation->dateFin); echo $oDate->toString('d-m-Y'); ?></div>
	</div>
	<div class="form-group">
	<table class="table table-striped">
		<tr>

			<th>Date de d&eacute;but de suivi</th>

			<th>Heure de d&eacute;but de suivi</th>

			<th>Date de fin de suivi</th>

			<th>Heure de fin de suivi</th>

			<th></th>
		</tr>
		<?php if($this->tDatessuivi):?>
			<?php foreach($this->tDatessuivi as $oDatessuivi):?>
			<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
	
				<td><?php $oDate= new plugin_date($oDatessuivi->dateDebut); echo $oDate->toString('d-m-Y');?></td>

				<td><?php echo $oDatessuivi->timeDebut;?></td>

				<td><?php $oDate= new plugin_date($oDatessuivi->dateFin); echo $oDate->toString('d-m-Y'); ?></td>

				<td><?php echo $oDatessuivi->timeFin;?></td>
	
				<td>
					<a class="btn btn-success" href="<?php echo $this->getLink ( 'datessuivi::edit', array ('id' => $oDatessuivi->getId () ) )?>">Edit</a>
					<a class="btn btn-danger" href="<?php echo $this->getLink ( 'datessuivi::delete', array ('id' => $oDatessuivi->getId () ) )?>">Delete</a>
				</td>
			</tr>	
			<?php endforeach;?>
		<?php else:?>
			<tr>
				<td colspan="3">Aucun suivi</td>
			</tr>
		<?php endif;?>
	</table>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Budget</label>
		<div class="col-sm-10"><?php echo $this->oSimulation->budget ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Délai</label>
		<div class="col-sm-10"><?php echo $this->oSimulation->delai ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Qualité</label>
		<div class="col-sm-10"><?php echo $this->oSimulation->qualite ?></div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<a class="btn btn-primary" href="<?php echo $this->getLink('datessuivi::new', array('idsimu'=>$this->oSimulation->id) ) ?>">New</a>
			<a class="btn btn-default" href="<?php echo $this->getLink('simulation::list')?>">Retour</a>
		</div>
	</div>
</form>
