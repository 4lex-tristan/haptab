<?php 
$oForm=new plugin_form($this->oSimulation);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('name',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Debut de la simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputDate('dateDebut',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Fin de la simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputDate('dateFin',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Budget</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('budget',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Délai</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('delai',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Qualité</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('simulation::list')?>">Annuler</a>
	</div>
</div>
</form>

