<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Nom</label>
		<div class="col-sm-10"><?php echo $this->oSimulation->name ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Debut de la simulation</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date( $this->oSimulation->dateDebut); echo $oDate->toString('d-m-Y'); ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Fin de la simulation</label>
		<div class="col-sm-10"><?php $oDate= new plugin_date( $this->oSimulation->dateFin); echo $oDate->toString('d-m-Y');?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Budget</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('budget',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Délai</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('delai',array('class'=>'form-control'))?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Qualité</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('qualite',array('class'=>'form-control'))?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('simulation::list')?>">Annuler</a>
	</div>
</div>
</form>
