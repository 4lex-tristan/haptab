<table class="table table-striped">
	<tr>
		
		<th>Nom</th>

		<th>Nombred&#039;&eacute;v&egrave;nements</th>

		<th>Debut de la simulation</th>

		<th>Fin de la simulation</th>
		
		<th>Budget ($)</th>
		
		<th>Délai (jours)</th>
		
		<th>Qualité (pts)</th>

		<th></th>
	</tr>
	<?php if($this->tSimulation):?>
		<?php foreach($this->tSimulation as $oSimulation):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oSimulation->name ?></td>

		<td><?php $oNbSuivi=model_datessuivi::getInstance()->getNbSuiviBySimu($oSimulation->id); echo $oNbSuivi->number ?></td>

		<td><?php $oDate= new plugin_date( $oSimulation->dateDebut); echo $oDate->toString('d-m-Y'); ?></td>

		<td><?php $oDate= new plugin_date($oSimulation->dateFin); echo $oDate->toString('d-m-Y'); ?></td>
		
		<td><?php echo $oSimulation->budget ?></td>
		
		<td><?php echo $oSimulation->delai ?></td>
		
		<td><?php echo $oSimulation->qualite ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('simulation::edit',array(
										'id'=>$oSimulation->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('simulation::delete',array(
										'id'=>$oSimulation->getId()
									) 
							)?>">Delete</a>

<a class="btn btn-default" href="<?php echo $this->getLink('simulation::show',array(
										'id'=>$oSimulation->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="5">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('simulation::new') ?>">New</a></p>

