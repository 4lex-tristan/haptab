<?php 
class module_simulation extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');		
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tSimulation=model_simulation::getInstance()->findAll();
		
		$oView=new _view('simulation::list');
		$oView->tSimulation=$tSimulation;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oSimulation=new row_simulation;
		
		$oView=new _view('simulation::new');
		$oView->oSimulation=$oSimulation;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oSimulation=model_simulation::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('simulation::edit');
		$oView->oSimulation=$oSimulation;
		$oView->tId=model_simulation::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _show(){
		$oSimulation=model_simulation::getInstance()->findById( _root::getParam('id') );
		$tDatessuivi=model_datessuivi::getInstance()->findBySimu(_root::getParam('id'));
		$iNbSuivi=model_datessuivi::getInstance()->getNbSuiviBySimu(_root::getParam('id'));
		
		$oView=new _view('simulation::show');
		$oView->oSimulation=$oSimulation;
		$oView->tDatessuivi=$tDatessuivi;
		$oView->nbSuivi=$iNbSuivi->number;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oSimulation=model_simulation::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('simulation::delete');
		$oView->oSimulation=$oSimulation;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oSimulation=new row_simulation;	
		}else{
			$oSimulation=model_simulation::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('name','dateDebut','dateFin','budget','delai','qualite');
		foreach($tColumn as $sColumn){
			$oSimulation->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oSimulation->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('simulation::list');
		}else{
			return $oSimulation->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oSimulation=model_simulation::getInstance()->findById( _root::getParam('id',null) );
				
		$oSimulation->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('simulation::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

