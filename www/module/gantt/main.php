<?php 
class module_gantt extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	/* #debutaction#
	public function _exampleaction(){
	
		$oView=new _view('examplemodule::exampleaction');
		
		$this->oLayout->add('main',$oView);
	}
	#finaction# */
	
	
	public function _index(){
		$oAccount=model_account::getInstance()->findById(_root::getParam('id'));
		$tTaches=model_taches::getInstance()->findAllBySim($oAccount->id_simulation);
		$oView=new _view('gantt::index');
		$oView->tTaches=$tTaches;
		$this->oLayout->add('main',$oView);
	}
	
	
	public function after(){
		$this->oLayout->show();
	}
	
	
}
