<table class="table table-striped">
	<tr>
		
		<th>Nom de l&#039;OS</th>

		<th>Nb. d&#039;ingenieurs</th>

		<th>Nb. de t&eacute;chniciens</th>

		<th>Dur&eacute;e de r&eacute;alisation</th>

		<th>Simulation</th>

		<th></th>
	</tr>
	<?php if($this->tSystemeexplotation):?>
		<?php foreach($this->tSystemeexplotation as $oSystemeexplotation):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oSystemeexplotation->nom ?></td>

		<td><?php echo $oSystemeexplotation->ingenieur ?></td>

		<td><?php echo $oSystemeexplotation->technicien ?></td>

		<td><?php echo $oSystemeexplotation->duree ?></td>

		<td><?php if(isset($this->tJoinmodel_simulation[$oSystemeexplotation->id_simulation])){ echo $this->tJoinmodel_simulation[$oSystemeexplotation->id_simulation];}else{ echo $oSystemeexplotation->id_simulation ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('systemeexplotation::edit',array(
										'id'=>$oSystemeexplotation->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('systemeexplotation::delete',array(
										'id'=>$oSystemeexplotation->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="6">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('systemeexplotation::new') ?>">New</a></p>

