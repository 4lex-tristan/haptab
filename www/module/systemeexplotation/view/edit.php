<?php 
$oForm=new plugin_form($this->oSystemeexplotation);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de l&#039;OS</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. d&#039;ingenieurs</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('ingenieur',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. de t&eacute;chniciens</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('technicien',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Dur&eacute;e de r&eacute;alisation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('duree',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_simulation',$this->tJoinmodel_simulation,array('class'=>'form-control'));?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('systemeexplotation::list')?>">Annuler</a>
	</div>
</div>
</form>

