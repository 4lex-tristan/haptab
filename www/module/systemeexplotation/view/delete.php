<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de l&#039;OS</label>
		<div class="col-sm-10"><?php echo $this->oSystemeexplotation->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. d&#039;ingenieurs</label>
		<div class="col-sm-10"><?php echo $this->oSystemeexplotation->ingenieur ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb. de t&eacute;chniciens</label>
		<div class="col-sm-10"><?php echo $this->oSystemeexplotation->technicien ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Dur&eacute;e de r&eacute;alisation</label>
		<div class="col-sm-10"><?php echo $this->oSystemeexplotation->duree ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oSystemeexplotation->id_simulation]?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('systemeexplotation::list')?>">Annuler</a>
	</div>
</div>
</form>
