<?php 
class module_systemeexplotation extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tSystemeexplotation=model_systemeexplotation::getInstance()->findAll();
		
		$oView=new _view('systemeexplotation::list');
		$oView->tSystemeexplotation=$tSystemeexplotation;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oSystemeexplotation=new row_systemeexplotation;
		
		$oView=new _view('systemeexplotation::new');
		$oView->oSystemeexplotation=$oSystemeexplotation;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oSystemeexplotation=model_systemeexplotation::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('systemeexplotation::edit');
		$oView->oSystemeexplotation=$oSystemeexplotation;
		$oView->tId=model_systemeexplotation::getInstance()->getIdTab();
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oSystemeexplotation=model_systemeexplotation::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('systemeexplotation::delete');
		$oView->oSystemeexplotation=$oSystemeexplotation;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oSystemeexplotation=new row_systemeexplotation;	
		}else{
			$oSystemeexplotation=model_systemeexplotation::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('nom','ingenieur','technicien','duree','id_simulation');
		foreach($tColumn as $sColumn){
			$oSystemeexplotation->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oSystemeexplotation->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('systemeexplotation::list');
		}else{
			return $oSystemeexplotation->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oSystemeexplotation=model_systemeexplotation::getInstance()->findById( _root::getParam('id',null) );
				
		$oSystemeexplotation->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('systemeexplotation::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

