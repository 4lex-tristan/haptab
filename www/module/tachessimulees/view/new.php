<?php 
$oForm=new plugin_form($this->oTachessimulees);
$oForm->setMessage($this->tMessage);
$oHTML = new plugin_html($this->oTachessimulees);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">IdSimulation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdSimulation',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">IdEquipe</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdEquipe',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">NumeroSuivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NumeroSuivi',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">IdTache</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdTache',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">NbHeureSup</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NbHeureSup',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Antecedents</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Antecedents',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Ingenieurs</label>
		<div class="col-sm-10"><?php echo $oHTML->getSelectMultiple('Ingenieurs',$this->tJoinmodel_ressourceshumainesIng,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire_Ing</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Stagiaire_Ing',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Techniciens</label>
		<div class="col-sm-10"><?php echo $oHTML->getSelectMultiple('Techniciens',$this->tJoinmodel_ressourceshumainesTech,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire_Tech</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Stagiaire_Tech',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">DureeCalcule</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('DureeCalcule',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('tachessimulees::list')?>">Annuler</a>
	</div>
</div>
</form>
