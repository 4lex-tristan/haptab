<?php 
$oForm=new plugin_form($this->oTachessimulees);
$oForm->setMessage($this->tMessage);
$oHTML = new plugin_html($this->oTachessimulees);
?>

<form class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Pays</label>
		<div class="col-sm-10"><?php echo  $this->tJoinmodel_contraintepays[$this->oTache->Pays];?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nom de la tache</label>
		<div class="col-sm-10"><?php echo $this->oTache->Nom?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Antécédents</label>
		<div class="col-sm-10"><?php 
		$tAntecedents=array();
		for($j=0;$j<=$this->iTachesMax;$j++){
			if ($j != $this->oTachessimulees->IdTache){
			$tAntecedents[]=$j;
			}
		}
		echo $oHTML->getSelectMultiple('Antecedents',$tAntecedents,$this->tTacheAffecte,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heures Supp.</label>
		<div class="col-sm-10"><?php 
		$tNbHSup=array();
			for($j=0;$j<=$this->oPays->MaxHeureSup;$j++){
				$tNbHSup[]=$j;
			}
			echo $oHTML->getSelect('NbHeureSup',$tNbHSup,$this->oTachessimulees->NbHeureSup,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Ingenieurs</label>
		<div class="col-sm-10"><?php echo $oHTML->getSelectMultiple('Ingenieurs',$this->tJoinmodel_ingenieur,$this->tIngeAffecte,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire Ingenieur</label>
		<div class="col-sm-10"><?php 
			$tStagIng=array();
			for($j=0;$j<=$this->oPays->NbStag;$j++){
				$tStagIng[]=$j;
			}
			echo $oHTML->getSelect('Stagiaire_Ing',$tStagIng,$this->oTachessimulees->Stagiaire_Ing,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Technicien</label>
		<div class="col-sm-10"><?php echo $oHTML->getSelectMultiple('Techniciens',$this->tJoinmodel_technicien,$this->tTechAffecte,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire Technicien</label>
		<div class="col-sm-10"><?php 
		$tStagTech=array();
		for($j=0;$j<=$this->oPays->NbStag;$j++){
			$tStagTech[]=$j;
		}
		echo $oHTML->getSelect('Stagiaire_Tech',$tStagTech,$this->oTachessimulees->Stagiaire_Tech,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb Ouvrier</label>
		<div class="col-sm-10"><?php echo $oHTML->getInputNumber('NbOuv',$this->oTachessimulees->NbOuv,array('class'=>'form-control'));?></div>
	</div>
	<?php echo '<input type="hidden" name="IdTache" value="'.$this->oTachessimulees->IdTache.'" />'
	//echo $oHTML->getInputHidden('IdTache',array('class'=>'form-control'))?>



<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-warning" value="Modifier" /> <a class="btn " href="<?php echo $this->getLink('tachessimulees::index') ?>">Retour </a>
	</div>
</div>
</form>
