<?php 
$oForm=new plugin_form($this->oTachessimulees);
$oForm->setMessage($this->tMessage);
$oHTML = new plugin_html($this->oTachessimulees);
?>
<form class="form-horizontal" action="" method="POST">

	<table class="table">
		<thread>
		<tr>
			<th>#</th>
			<th>Pays</th>
			<th>Nom de la Tache</th>
			<th>Antécédants</th>
			<th>Heures Supp.</th>
			<th>Ingenieurs</th>
			<th>Stagiaire Ingenieur</th>
			<th>Technicien</th>
			<th>Stagiaire Technicien</th>
			<th>Nb Ouvrier</th>
		</tr>
		</thread>
		<?php
		$i=0;
		foreach ($this->tTaches as $oTache):?>
		<tr>
			<td><?php $iTacheID =$oTache->id;
			echo $iTacheID;  
			echo' <input type="hidden" name="IdTache['.$i.']" value="'.$iTacheID.'" />';?></td>
			<td><?php echo  $this->tJoinmodel_contraintepays[$oTache->Pays];?></td>
			<td><?php echo $oTache->Nom?></td>
			<td><?php $tAntecedents=array();
			for($j=0;$j<=$this->iTachesMax;$j++){
				if ($j != $oTache->id ){
				$tAntecedents[]=$j;
				}
			}
			// echo $oForm->getInputText('Antecedents['.$i.']',array('class'=>'form-control'));
			echo $oHTML->getSelectMultiple('Antecedents['.$i.']',$tAntecedents,array('class'=>'form-control'));?>
			</td>
			<td><?php $oPays= model_contraintepays::getInstance()->findById($oTache->Pays);
			$tHeuresSup=array();
			for($j=0;$j<=$oPays->MaxHeureSup;$j++){
				$tHeuresSup[]=$j;
			}
			 echo $oHTML->getSelect('NbHeureSup['.$i.']',$tHeuresSup,array('class'=>'form-control'));
			//echo $oForm->getInputText('NbHeureSup['.$i.']',array('class'=>'form-control'))?></td>
			<td><?php echo $oHTML->getSelectMultiple('Ingenieurs['.$i.']',model_ressourceshumaines::getInstance()->getSelectIngByPays($oTache->Pays),array('class'=>'form-control'));?></td>
			<td><?php 
			$tStagIng=array();
			for($j=0;$j<=$oPays->NbStag;$j++){
				$tStagIng[]=$j;
			}
			echo $oHTML->getSelect('Stagiaire_Ing['.$i.']',$tStagIng,array('class'=>'form-control'));
			//echo $oForm->getInputText('Stagiaire_Ing['.$i.']',array('class'=>'form-control'))?></td>
			<td><?php echo $oHTML->getSelectMultiple('Techniciens['.$i.']',model_ressourceshumaines::getInstance()->getSelectTechByPays($oTache->Pays),array('class'=>'form-control'));?></td>
			<td><?php 
			$tStagTech=array();
			for($j=0;$j<=$oPays->NbStag;$j++){
				$tStagTech[]=$j;
			}
			echo $oHTML->getSelect('Stagiaire_Tech['.$i.']',$tStagTech,array('class'=>'form-control'));
			//echo $oForm->getInputText('Stagiaire_Tech['.$i.']',array('class'=>'form-control'))?></td>
			<td><?php echo $oHTML->getInputNumber('NbOuv['.$i.']',array('class'=>'form-control','min'=>'0','max'=>'20','step'=>'1'));
			/*if($oPays->NbOuvrier != 0){
				echo $oForm->getInputText('NbOuv['.$i.']',array('class'=>'form-control'));
			}else{
				$NbOuv[$i]=0;
				$oForm->getInputHidden('NbOuv['.$i.']');
				echo '0';
			} */?></td>
		</tr>
		<?php $i++; ?>
		<?php endforeach;?>
	</table>

<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="submit" class="btn btn-success" value="Soumettre" />
		</div>
	</div>
</form>
