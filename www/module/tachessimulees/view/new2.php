<?php 
$oForm=new plugin_form($this->oTachessimulees);
$oForm->setMessage($this->tMessage);
?>
<!--  <form method="post" action="index.php?section=groupe&amp;subsection=tasksSelec"> -->
<form  class="form-horizontal" action="" method="POST" >
		<table>
			<tr>
				<th>Numero</th>
				<th>Pays</th>
				<th>Nom de la Tache</th>
				<th>Antécédants</th>
				<th>Heures Supplémentaires</th>
				<th>Ingenieurs</th>
				<th>Stagiaire Ingenieur</th>
				<th>Technicien</th>
				<th>Stagiaire Technicien</th>
				<th>Nb Ouvrier</th>
			</tr>
			<?php foreach ($Tasks as $task){?>
				<tr>
					<td><?php echo $task['id'];?></td>
					<td><?php echo $task['Pays']?></td>
					<td><?php echo $task['Nom']?></td>
					<td><?php echo'<select name="Antecedent['.$task['id'].'][]" 
						class="Antecedent" multiple>'; 
						for($i=0;$i<=$nbTasks;$i++){
							if ($i!= $task['id'])
								echo '<option>'.$i.'</option>'; 
						}
						echo ' </select>';?>
					</td>
					<td>
						<?php echo'<select name="HeureSup['.$task['id'].']" 
						class="HeureSup">';
						for($i=0;$i<=$heureSup[$task['Pays']];$i++){
							echo '<option>'.$i.'</option>';
						}
						echo '</select>'?>
					</td>
					<td>
						<?php echo'<select name="Ingenieur['.$task['id'].'][]" size="7"
						class="Ingenieur" multiple>';
						foreach ($ingsByCountry[$task['Pays']] as $ing){
							echo '<option>'.$ing.'</option>';
						}?>
						
					</td>
					<!-- 
					<td><select name="Ingenieur[${Tache.idtache}][]" size="7"
						class="Ingenieur" multiple>
							<c:forEach var="Ing" items="${ Inge }">
								<c:if test="${ Tache.pays == Ing.pays }">
									<option><c:out value="${ Ing.nom }" /></option>
								</c:if>
								<!-- <option selected> pour afficher les ingenieur deja selectionné  -->
							<!--</c:forEach>
					</select></td>
					<td><select name="NbStagIng[${Tache.idtache}]" size="1">
							<c:forEach var="i" begin="0" end="4" step="1">
								<!-- nombre de stagiaire depend du pays  -->
								<!-- <option><c:out value="${ i }" /></option>
							</c:forEach>
					</select></td>
					<td><select name="Technicien[${Tache.idtache}][]" size="7"
						class=" Technicien" multiple>
							<c:forEach var="Tech" items="${ Techn }">
								<c:if test="${ Tache.pays == Tech.pays }">
									<option><c:out value="${ Tech.nom }" /></option>
								</c:if>
								<!--  <option selected> pour afficher les techniciens deja selectionné par l'equipe -->
							<!--  </c:forEach>
					</select></td>
					<td><select name="NbStagTech[${Tache.idtache}]" size="1">
							<c:forEach var="i" begin="0" end="4" step="1">
								<!-- nombre de stagiaire depend du pays  -->
								<!--  <option><c:out value="${ i }" /></option>
							</c:forEach>
					</select></td>-->
					<td><input type="number" name="NbOuvrier[${Tache.idtache}]"
						id="NbOuvrier" min="0" value="0" /></td> 

				</tr>
			<?php }unset($task)?>
		</table>
		<input type="hidden" name="newTasksSelec" value="TURE" />
		<input type="submit" class="btn btn-success" value="Modifier" />
	</form>
<!-- <form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">IdSimulation</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdSimulation',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">IdEquipe</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdEquipe',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">NumeroSuivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NumeroSuivi',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">IdTache</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('IdTache',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">NbHeureSup</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NbHeureSup',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Antecedents</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Antecedents',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Ingenieurs</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('Ingenieurs',$this->tJoinmodel_ressourceshumaines,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire_Ing</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Stagiaire_Ing',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Techniciens</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('Techniciens',$this->tJoinmodel_ressourceshumaines,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Stagiaire_Tech</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Stagiaire_Tech',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">DureeCalcule</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('DureeCalcule',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('tachessimulees::list')?>">Annuler</a>
	</div>
</div>
</form>
 -->