<form  class="form-horizontal" action="" method="POST" >

	<table class="table">
		<thread>
		<tr>
			<th>#</th>
			<th>Pays</th>
			<th>Nom de la Tache</th>
			<th>Antécédents</th>
			<th>Heures Supp.</th>
			<th>Ingenieurs</th>
			<th>Stagiaire Ingenieur</th>
			<th>Technicien</th>
			<th>Stagiaire Technicien</th>
			<th>Nb Ouvrier</th>
		</tr>
		</thread>
		</thread>
		<?php
		foreach ($this->tTachessimulees as $oTacheSimu):?>
		<tr>
			<td><?php echo $oTacheSimu->id; ?></td>
			<td><?php $oTache=model_taches::getInstance()->findById($oTacheSimu->IdTache);
			echo $this->tJoinmodel_contraintepays[$oTache->Pays]; ?></td>
			<td><?php echo $oTache->Nom?></td>
			<td><?php $tAntecedents=model_antecedent::getInstance()->findAllById($oTacheSimu->id);
			$firstAnt= true;
			$sAnt = '';
			foreach ($tAntecedents as $oAnte){
				if ($firstAnt){
					$sAnt = ''.$oAnte->IdTache;
					$firstAnt= false;
				}else{
					$sAnt = $sAnt.', '.$oAnte->IdTache;
				}
			}
			echo $sAnt;
			//$oTacheSimu->Antecedents;?></td>
			<td><?php $oTacheSimu->NbHeureSup;?></td>
			<td><?php $tInge=model_ingenieuraffecte::getInstance()->findAllByIdTache($oTacheSimu->id);
			$firstIng= true;
			$sIng = '';
			foreach ($tInge as $oInge){
				$oRH=model_ressourceshumaines::getInstance()->findById($oInge->IdIng);
				if ($firstIng){
					$sIng = ''.$oRH->nom;
					$firstIng= false;
				}else{
					$sIng = $sIng.', '.$oRH->nom;
				}
			}
			echo $sIng;
			?></td>
			<td><?php echo  $oTacheSimu->Stagiaire_Ing;?></td>
			<td><?php $tTech=model_technicienaffecte::getInstance()->findAllByIdTache($oTacheSimu->id);
			$firstTech= true;
			$sTech = '';
			foreach ($tTech as $oTech){
				$oRH=model_ressourceshumaines::getInstance()->findById($oTech->IdTech);
				if ($firstTech){
					$sTech = ''.$oRH->nom;
					$firstTech= false;
				}else{
					$sTech = $sTech.', '.$oRH->nom;
				}
			}
			echo $sTech;
			?></td>
			<td><?php echo $oTacheSimu->Stagiaire_Tech; ?></td>
			<td><?php echo $oTacheSimu->NbOuv; ?></td>
		</tr>
		<?php endforeach;?>
	</table>
	<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<a class="btn btn-link" href="<?php echo $this->getLink('tdbGroupe::index')?>">Retour</a>
	</div>
</div>
</form>