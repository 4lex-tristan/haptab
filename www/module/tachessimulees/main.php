<?php
class module_tachessimulees extends abstract_module {
	public function before() {
		_root::getAuth()->enable();
		$this->oLayout = new _layout ( 'bootstrap' );
		
		$this->oLayout->addModule ( 'menu', 'menuSimulation::index' );
	}
	public function _index() {
		if (model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null && model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] )) { // verifier que les composants de la tablette sont bien selectionné sinon le calcul de na qualité ne peut pas se faire
			// on considere que la page par defaut est la page new si rien n'est en memoire pour le suivi courent
			// sinon afficher la liste des decisions si elle n'a pas été validé sinon afficher les details des resultats.
			if (model_parametrevalidation::getInstance ()->tasksAreValidated ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				_root::redirect ( 'tachessimulees::detail&suivi=' . $_SESSION ['suivi'] );
			} elseif (model_tachessimulees::getInstance ()->isFulfilled ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				_root::redirect ( 'tachessimulees::editlist&suivi=' . $_SESSION ['suivi'] );
			} else {
				$this->_new ();
			}
		} else {
			$this->_noaccess ();
		}
		
		
	}
	public function _noaccess() {
		$oView = new _view ( 'tachessimulees::noaccess' );
		$this->oLayout->add ( 'main', $oView );
	}
	public function _new() {
		if ($_SESSION ['suivi'] == - 1) {
			$oView = new _view ( 'tachessimulees::nosuivi' );
		} else {
		$tMessage = $this->processSave ();
		
		$oTachessimulees = new row_tachessimulees ();
		$tTaches = model_taches::getInstance ()->findAllBySim ( $_SESSION ['simulation'] );
		$iTachesMax = count ( $tTaches );
		
		$oView = new _view ( 'tachessimulees::newB' );
		$oView->tJoinmodel_contraintepays = model_contraintepays::getInstance ()->getSelect ();
		$oView->tTaches = $tTaches;
		$oView->iTachesMax = $iTachesMax;
		$oView->oTachessimulees = $oTachessimulees;
		
		$oPluginXsrf = new plugin_xsrf ();
		$oView->token = $oPluginXsrf->getToken ();
		$oView->tMessage = $tMessage;
		}
		
		$this->oLayout->add ( 'main', $oView );
	}
	public function _editlist() {
		$tMessage=$this->ProcessValidation();
		
		$tTaches = model_taches::getInstance ()->findAllBySim ( $_SESSION ['simulation'] );
		$iTachesMax = count ( $tTaches );
		
		$tTachessimulees = model_tachessimulees::getInstance ()->findBySuivi ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], _root::getParam ( 'suivi' ) );
		$tTacheSim = array();
		foreach($tTachessimulees as $oTacheSimulee){
			$tAntecedents=model_antecedent::getInstance()->findAllById($oTacheSimulee->id);
			$firstAnt= true;
			$sAnt = '';
			foreach ($tAntecedents as $oAnte){
				if ($firstAnt){
					$sAnt = ''.$oAnte->IdTache;
					$firstAnt= false;
				}else{
					$sAnt = $sAnt.', '.$oAnte->IdTache;
				}
			}
			$oTacheSimulee->Antecedents = $sAnt;
			$tTacheSim[] = $oTacheSimulee;
		}

		
		$oView = new _view ( 'tachessimulees::editList' );
		
		$oView->tTaches = $tTaches;
		$oView->tTachessimulees = $tTacheSim;
		$oView->iTachesMax = $iTachesMax;
		
		$oView->tJoinmodel_contraintepays = model_contraintepays::getInstance ()->getSelect ();
		
		$oPluginXsrf = new plugin_xsrf ();
		$oView->token = $oPluginXsrf->getToken ();
		$oView->tMessage = $tMessage;
		
		$this->oLayout->add ( 'main', $oView );
	}
	public function _edit() {
		if ($_SESSION ['suivi'] == - 1) {
			$oView = new _view ( 'tachessimulees::nosuivi' );
		} else {
		$tMessage = $this->processSaveEdit ();
		
		$oTachessimulees = model_tachessimulees::getInstance ()->findById ( _root::getParam ( 'id' ) );
		$oTache = model_taches::getInstance ()->findById ( $oTachessimulees->IdTache );
		$oPays = model_contraintepays::getInstance ()->findById ( $oTache->Pays );
		$tTaches = model_taches::getInstance ()->findAllBySim ( $_SESSION ['simulation'] );
		$iTachesMax = count ( $tTaches );
		
		$oView = new _view ( 'tachessimulees::edit' );
		$oView->oTachessimulees = $oTachessimulees;
		$oView->oTache = $oTache;
		$oView->iTachesMax = $iTachesMax;
		$oView->oPays = $oPays;
		
		
		$tJoinmodel_ingenieur_affecte= model_ingenieuraffecte::getInstance()-> findAllByIdTache($oTachessimulees->id);
		$tIngeAffecte = array();
		foreach ($tJoinmodel_ingenieur_affecte as $oIngeAffecte){
			$tIngeAffecte[] = $oIngeAffecte->IdIng;
		}
		$oView->tIngeAffecte = $tIngeAffecte;
		
		$tJoinmodel_technicien_affecte= model_technicienaffecte::getInstance()-> findAllByIdTache($oTachessimulees->id);
		$tTechAffecte = array();
		foreach ($tJoinmodel_technicien_affecte as $oTechAffecte){
			$tTechAffecte[] = $oTechAffecte->IdTech;
		}
		$oView->tTechAffecte = $tTechAffecte;
		
		$tJoinmodel_antecedents= model_antecedent::getInstance()-> findAllById($oTachessimulees->id);
		$tTacheAffecte = array();
		foreach ($tJoinmodel_antecedents as $oAntecedentAffecte){
			$tTacheAffecte[] = $oAntecedentAffecte->IdTache;
		}
		$oView->tTacheAffecte = $tTacheAffecte;
		
		$oView->tJoinmodel_ingenieur = model_ressourceshumaines::getInstance ()->getSelectIngByPays ( $oTache->Pays );
		$oView->tJoinmodel_technicien = model_ressourceshumaines::getInstance ()->getSelectTechByPays ( $oTache->Pays );
		$oView->tJoinmodel_contraintepays = model_contraintepays::getInstance ()->getSelect ();
		
		$oPluginXsrf = new plugin_xsrf ();
		$oView->token = $oPluginXsrf->getToken ();
		$oView->tMessage = $tMessage;
		}
		
		$this->oLayout->add ( 'main', $oView );
	}
	public function _detail() {
		$tTachessimulees = model_tachessimulees::getInstance ()->findBySuivi ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], _root::getParam ( 'suivi' ) );
		
		if (_root::getRequest ()->isPost ()) {
			
		}
		
		$oView = new _view ( 'tachessimulees::detail' );
		$oView->tJoinmodel_contraintepays = model_contraintepays::getInstance ()->getSelect ();
		$oView->tTachessimulees = $tTachessimulees;
		$this->oLayout->add ( 'main', $oView );
	}

	private function processSaveEdit() {
		if (! _root::getRequest ()->isPost ()) { // si ce n'est pas une requete POST on ne soumet pas
			return null;
		}

		$oPluginXsrf = new plugin_xsrf ();
		if (! $oPluginXsrf->checkToken ( _root::getParam ( 'token' ) )) { // on verifie que le token est valide
			return array (
					'token' => $oPluginXsrf->getMessage () 
			);
		}


		$oTachessimulees=model_tachessimulees::getInstance()->findById( _root::getParam('id') );
		$oTachessimulees->IdSimulation = $_SESSION ['simulation'];
		$oTachessimulees->IdEquipe = $_SESSION ['nEquipe'];
		$oTachessimulees->NumeroSuivi = $_SESSION ['suivi'];

		// *** calcul de la durée 'DureeCalcule'
		$oTacheInit = model_taches::getInstance ()->findById ( _root::getParam ( 'IdTache' ) );
		$oPays = model_contraintepays::getInstance ()->findById ( $oTacheInit->Pays );

		// calcul de la rapidité des differents acteurs
		$tIngenieurs = _root::getParam ( 'Ingenieurs', null );
		$iRapidIng = 0;
		if (isset ( $tIngenieurs )) {
			foreach ( $tIngenieurs as $oInge ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
				$iRapidIng = $iRapidIng + $oRH->rapidite;
			}
		}
		$tTechniciens = _root::getParam ( 'Techniciens', null );
		$iRapidTech = 0;
		if (isset ( $tTechniciens )) {
			foreach ( $tTechniciens as $oTech ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oTech );
				$iRapidTech = $iRapidTech + $oRH->rapidite;
			}
		}
		
		// calcul de la Compétence des differents acteurs
		$iCompIng = 0;
		if (isset ( $tIngenieurs )) {
			foreach ( $tIngenieurs as $oInge ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
				$iCompIng = $iCompIng + $oRH->competence;
			}
			$iCompIng= $iCompIng /sizeof($tIngenieurs);
		}
		$iCompTech = 0;
		if (isset ( $tTechniciens )) {
			foreach ( $tTechniciens as $oTech ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oTech );
				$iCompTech = $iCompTech + $oRH->competence;
			}
			$iCompTech = $iCompTech/ sizeof($tTechniciens );
		}
		
		// application de la penalité de rapidité pour les ouvriers interimaires
		if (is_int(_root::getParam ( 'NbOuv' )) && _root::getParam ( 'NbOuv' ) > $oPays->NbOuvrier) {
			$iRapidOuv = 100 * $oPays->NbOuvrier + 90 * (_root::getParam ( 'NbOuv' ) - $oPays->NbOuvrier);
			$iCompOuv = 100 * $oPays->NbOuvrier + 95 * (_root::getParam ( 'NbOuv' ) - $oPays->NbOuvrier);
		} elseif(is_int(_root::getParam ( 'NbOuv' )) ){
			$iRapidOuv = _root::getParam ( 'NbOuv' ) * 100;
			$iCompOuv = _root::getParam ( 'NbOuv' ) *100;
		}else{
			$iRapidOuv = 0;
			$iCompOuv = 0;
		}

		// ajout des delais supplementaires induit par certain choix de composants
		if (model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
			$oResultat = model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
			if (_root::getParam ( 'IdTache' ) == 4) {
				// si la taches est le prototypage de la tablette
				$iDureeInit = $oTacheInit->Duree_initiale + $oResultat->duree_sup_proto;
			} elseif (_root::getParam ( 'IdTache' ) == 20) {
				// si la tache est la production de la serie de tablette
				$iDureeInit = $oTacheInit->Duree_initiale + $oResultat->duree_sup_prod;
			} else {
				$iDureeInit = $oTacheInit->Duree_initiale;
			}
		} else {
			$iDureeInit = $oTacheInit->Duree_initiale;
		}
		
		$oTachessimulees->DureeCalculee = $iDureeInit * ($oTacheInit->Nb_Ing_Init + $oTacheInit->Nb_Tech_Init + $oTacheInit->Nb_Ouv_Init) / ($iRapidIng / 100 + $iRapidTech / 100 + $iRapidOuv / 100); // a verifier
		$oTachessimulees->DureeFinale = ceil ( $oTachessimulees->DureeCalculee );
		$tResultat= model_resultat::getInstance()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		$oTachessimulees->Qualite = $tResultat->qualite_mp * ($iCompIng/100 + $iCompTech +$iCompOuv/100) ; 
		
		
		// calcul du cout
		$iCoutInge = 0;
		if (isset ( $tIngenieurs )) {
			foreach ( $tIngenieurs as $oInge ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
				$iCoutInge = $iCoutInge + $oTachessimulees->DureeFinale * $oPays->CoutHoraireInge * (100 + $oRH->prime) / 100; // devise locale
			}
		}
		$iCoutTech = 0;
		if (isset ( $tTechniciens )) {
			foreach ( $tTechniciens as $oInge ) {
				$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
				$iCoutTech = $iCoutTech + $oTachessimulees->DureeFinale * $oPays->CoutHoraireTech * (100 + $oRH->prime) / 100; // devise locale
			}
		}
		if (_root::getParam ( 'NbOuv' ) != 0) {
			$iCoutOuv = $oTachessimulees->DureeFinale * $oPays->CoutHoraireOuvrier * _root::getParam ( 'NbOuv' );  //devise locale
		} else {
			$iCoutOuv = 0;
		}
		
		$oTachessimulees->cout = $iCoutInge + $iCoutTech + $iCoutOuv;
		
		$tColumn = array (
				'IdTache',
				'NbHeureSup',
				'Stagiaire_Ing',
				'Stagiaire_Tech',
				'NbOuv' 
		);

		foreach ( $tColumn as $sColumn ) {
			if($sColumn == 'NbOuv' &&  $oTachessimulees->NbOuv){
				$oTachessimulees->$sColumn = 0;
			}else{
				$oTachessimulees->$sColumn = _root::getParam ( $sColumn );
			}
		}

		if ($oTachessimulees->save ()) {
			// une fois enregistre, on enregistre les antécedents, les ingénieurs et les techniciens
			$tIngenieurs = _root::getParam ( 'Ingenieurs', null );
			if (isset ( $tIngenieurs )) {
				$tIngeAffecte = model_ingenieuraffecte::getInstance ()->findAllByIdTache($oTachessimulees->id);
				foreach($tIngeAffecte as $oIngeAffecte){
					$oIngeAffecte->delete();
				}
				foreach ( $tIngenieurs as $oInge ) {
					$oIngeAffecte = new row_ingenieuraffecte ();
					$oIngeAffecte->IdIng = $oInge;
					$oIngeAffecte->IdTacheSimul = $oTachessimulees->id;
					if ($oIngeAffecte->save ()) {
					} else {
						return $oIngeAffecte->getListError ();
					}
				}
			}
			$tTechniciens = _root::getParam ( 'Techniciens', null );
			if (isset ( $tTechniciens )) {
				$tTechAffecte = model_technicienaffecte::getInstance ()->findAllByIdTache($oTachessimulees->id);
				foreach($tTechAffecte as $oTechAffecte){
					$oTechAffecte->delete();
				}
				foreach ( $tTechniciens as $oTech ) {
					$oTechAffecte = new row_technicienaffecte ();
					$oTechAffecte->IdTech = $oTech;
					$oTechAffecte->IdTacheSimul = $oTachessimulees->id;
					if ($oTechAffecte->save ()) {
					} else {
						return $oTechAffecte->getListError ();
					}
				}
			}
			$tAnt = _root::getParam ( 'Antecedents', null );
			if (isset ( $tAnt )) {
				$tAnteAffecte = model_antecedent::getInstance ()->findAllById($oTachessimulees->id);
				foreach($tAnteAffecte as $oAnteAffecte){
					$oAnteAffecte->delete();
				}
				foreach ( $tAnt as $oAnt ) {
					$oTacheAffecte = new row_antecedent ();
					$oTacheAffecte->IdTache = $oAnt;
					$oTacheAffecte->IdTacheSimule = $oTachessimulees->id;
					if ($oTacheAffecte->save ()) {
					} else {
						return $oTacheAffecte->getListError ();
					}
				}
			}
		} else {
			return $oTachessimulees->getListError ();
		}

		// redirection vers la page recapitulative
		_root::redirect ( 'tachessimulees::index' );
	}
	private function processSave() {
		if (! _root::getRequest ()->isPost ()) { // si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf = new plugin_xsrf ();
		if (! $oPluginXsrf->checkToken ( _root::getParam ( 'token' ) )) { // on verifie que le token est valide
			return array (
					'token' => $oPluginXsrf->getMessage () 
			);
		}
		
		$nbTachesSoumises = sizeof ( $_POST ['NbHeureSup'] ); // il n'y a q'une valeur par tache --> nb de valeur de "NbHeureSup " = nb de taches soumises
		for($i = 0; $i <= $nbTachesSoumises - 1; $i ++) {
			$iId = _root::getParam ( 'id', null );
			if ($iId == null) {
				$oTachessimulees = new row_tachessimulees ();
			} else {
				$oTachessimulees = model_tachessimulees::getInstance ()->findById ( _root::getParam ( 'id', null ) );
			}
			$oTachessimulees->IdSimulation = $_SESSION ['simulation'];
			$oTachessimulees->IdEquipe = $_SESSION ['nEquipe'];
			$oTachessimulees->NumeroSuivi = $_SESSION ['suivi'];
			
			// *** calcul de la durée 'DureeCalcule'
			
			$oTacheInit = model_taches::getInstance ()->findById ( _root::getParam ( 'IdTache' )[$i] );
			$oPays = model_contraintepays::getInstance ()->findById ( $oTacheInit->Pays );
			
			// calcul de la rapidité des differents acteurs
			$tIngenieurs = _root::getParam ( 'Ingenieurs', null );
			$iRapidIng = 0;
			if (isset ( $tIngenieurs[$i] )) {
				foreach ( $tIngenieurs[$i] as $oInge ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
					$iRapidIng = $iRapidIng + $oRH->rapidite;
				}
			}
			$tTechniciens = _root::getParam ( 'Techniciens', null );
			$iRapidTech = 0;
			if (isset ( $tTechniciens [$i] )) {
				foreach ( $tTechniciens [$i] as $oTech ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oTech );
					$iRapidTech = $iRapidTech + $oRH->rapidite;
				}
			}
			
			// calcul de la Compétence des differents acteurs
			$iCompIng = 0;
			if (isset ( $tIngenieurs[$i] )) {
				foreach ( $tIngenieurs[$i] as $oInge ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
					$iCompIng = $iCompIng + $oRH->competence;
				}
				$iCompIng= $iCompIng /sizeof($tIngenieurs[$i]);
			}
			$iCompTech = 0;
			if (isset ( $tTechniciens[$i] )) {
				foreach ( $tTechniciens[$i] as $oTech ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oTech );
					$iCompTech = $iCompTech + $oRH->competence;
				}
				$iCompTech = $iCompTech/ sizeof($tTechniciens [$i]);
			}
			
			// application de la penalité de rapidité pour les ouvriers interimaires
			if (is_int(_root::getParam ( 'NbOuv' )[$i]) && _root::getParam ( 'NbOuv' )[$i] > $oPays->NbOuvrier) {
				$iRapidOuv = 100 * $oPays->NbOuvrier + 90 * (_root::getParam ( 'NbOuv' )[$i] - $oPays->NbOuvrier);
				$iCompOuv = 100 * $oPays->NbOuvrier + 95 * (_root::getParam ( 'NbOuv' )[$i] - $oPays->NbOuvrier);
			} elseif(is_int(_root::getParam ( 'NbOuv' )[$i]) ){
				$iRapidOuv = _root::getParam ( 'NbOuv' )[$i] * 100;
				$iCompOuv = _root::getParam ( 'NbOuv' )[$i] *100;
			}else{
				$iRapidOuv = 0;
				$iCompOuv = 0;
			}
			
			// ajout des delais supplementaires induit par certain choix de composants
			if (model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
				$oResultat = model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
				if (_root::getParam ( 'IdTache' )[$i] == 4) {
					// si la taches est le prototypage de la tablette
					$iDureeInit = $oTacheInit->Duree_initiale + $oResultat->duree_sup_proto;
				} elseif (_root::getParam ( 'IdTache' )[$i] == 20) {
					// si la tache est la production de la serie de tablette
					$iDureeInit = $oTacheInit->Duree_initiale + $oResultat->duree_sup_prod;
				} else {
					$iDureeInit = $oTacheInit->Duree_initiale;
				}
			} else {
				$iDureeInit = $oTacheInit->Duree_initiale;
			}
			
			$oTachessimulees->DureeCalculee = $iDureeInit * ($oTacheInit->Nb_Ing_Init + $oTacheInit->Nb_Tech_Init + $oTacheInit->Nb_Ouv_Init) / ($iRapidIng / 100 + $iRapidTech / 100 + $iRapidOuv / 100); // a verifier
			$oTachessimulees->DureeFinale = ceil ( $oTachessimulees->DureeCalculee );
			$tResultat= model_resultat::getInstance()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
			$oTachessimulees->Qualite = $tResultat->qualite_mp * ($iCompIng/100 + $iCompTech +$iCompOuv/100) ; 
			
			
			// calcul du cout
			$iCoutInge = 0;
			if (isset ( $tIngenieurs [$i] )) {
				foreach ( $tIngenieurs [$i] as $oInge ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
					$iCoutInge = $iCoutInge + $oTachessimulees->DureeFinale * $oPays->CoutHoraireInge * (100 + $oRH->prime) / 100; // devise locale
				}
			}
			$iCoutTech = 0;
			if (isset ( $tTechniciens [$i] )) {
				foreach ( $tTechniciens [$i] as $oInge ) {
					$oRH = model_ressourceshumaines::getInstance ()->findById ( $oInge );
					$iCoutTech = $iCoutTech + $oTachessimulees->DureeFinale * $oPays->CoutHoraireTech * (100 + $oRH->prime) / 100; // devise locale
				}
			}
			if (_root::getParam ( 'NbOuv' )[$i] != 0) {
				$iCoutOuv = $oTachessimulees->DureeFinale * $oPays->CoutHoraireOuvrier * _root::getParam ( 'NbOuv' )[$i]; // devise locale
			} else {
				$iCoutOuv = 0;
			}
			
			$oTachessimulees->cout = $iCoutInge + $iCoutTech + $iCoutOuv;
			
			$tColumn = array (
					'IdTache',
					'NbHeureSup',
					'Stagiaire_Ing',
					'Stagiaire_Tech',
					'NbOuv' 
			);
			foreach ( $tColumn as $sColumn ) {
				if($sColumn == 'NbOuv'  ){
					$oTachessimulees->$sColumn = 0;
				}else{
					$oTachessimulees->$sColumn = _root::getParam ( $sColumn )[$i];
				}
				
				//is_int(_root::getParam ( $sColumn )[$i]) ? $oTachessimulees->$sColumn = _root::getParam ( $sColumn )[$i] : $oTachessimulees->$sColumn = 0;
			}
			
			if ($oTachessimulees->save ()) {
				// une fois enregistre, on enregistre les antécedents, les ingénieurs et les techniciens
				$tIngenieurs = _root::getParam ( 'Ingenieurs', null );
				if (isset ( $tIngenieurs [$i] )) {
					foreach ( $tIngenieurs [$i] as $oInge ) {
						$oIngeAffecte = new row_ingenieuraffecte ();
						$oIngeAffecte->IdIng = $oInge;
						$oIngeAffecte->IdTacheSimul = $oTachessimulees->id;
						if ($oIngeAffecte->save ()) {
						} else {
							return $oIngeAffecte->getListError ();
						}
					}
				}
				$tTechniciens = _root::getParam ( 'Techniciens', null );
				if (isset ( $tTechniciens [$i] )) {
					foreach ( $tTechniciens [$i] as $oTech ) {
						$oTechAffecte = new row_technicienaffecte ();
						$oTechAffecte->IdTech = $oTech;
						$oTechAffecte->IdTacheSimul = $oTachessimulees->id;
						if ($oTechAffecte->save ()) {
						} else {
							return $oTechAffecte->getListError ();
						}
					}
				}
				$tAnt = _root::getParam ( 'Antecedents', null );
				if (isset ( $tAnt [$i] )) {
					foreach ( $tAnt [$i] as $oAnt ) {
						$oTacheAffecte = new row_antecedent (); // table de liaison a faire
						$oTacheAffecte->IdTache = $oAnt;
						$oTacheAffecte->IdTacheSimule = $oTachessimulees->id;
						if ($oTacheAffecte->save ()) {
						} else {
							return $oTacheAffecte->getListError ();
						}
					}
				}
			} else {
				return $oTachessimulees->getListError ();
			}
		}
		// redirection vers la page recapitulative
		_root::redirect ( 'tachessimulees::index' );
	}
	private function ProcessValidation(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne valide pas
			return null;
		}
	
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
		
		if (model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
			$oValidation = model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		} else {
			$oValidation = new row_parametrevalidation ();
		}
			
		$oValidation->id_equipe = $_SESSION ['nEquipe'];
		$oValidation->id_simu = $_SESSION ['simulation'];
		$oValidation->suivi = $_SESSION ['suivi'];
		$oValidation->tache_valid = 1;
		if ($oValidation->save ()) {
			$tTachessimulees = model_tachessimulees::getInstance ()->findBySuivi ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
			//****** calcul du cout total de la simulation
			if (model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
				$oResultat = model_resultat::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
			} else {
				$oResultat = new row_resultat ();
			}
			$CoutMO = 0;
			foreach ( $tTachessimulees as $oTachesSimulee ) {
				$oTache = model_taches::getInstance ()->findById ( $oTachesSimulee->IdTache );
				$oPays = model_contraintepays::getInstance ()->findById ( $oTache->Pays );
				$CurrencyRate = $oPays->ConversionDollarLocal;
				$qualTotTaches = $oTachesSimulee->Qualite;
				$CoutMO = $CoutMO + $CurrencyRate * $oTachesSimulee->Cout;
			}
			$oResultat->cout_mo = $CoutTotal;
			$oResultat->qualite_tot=$oResultat->qualite_mp + $qualTotTaches;
			$oResultat->cout_tot = $oResultat->cout_mp + $CoutMO;
		
			//**** calcul de la duréé du projet******
			$tTaches = model_taches::getInstance ()->findAllBySim ( $_SESSION ['simulation'] );
			// calcul de la table des antecedence
			$nbTaches = sizeof ( $tTaches );
			foreach ( $tTaches as $oTache ) {
				for($i = 1; $i <= $nbTaches; $i ++) {
					if ($oTache->id != $i) {
						$oTacheSimulee = model_tachessimulees::getInstance ()->findByIdTache ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'], $oTache->id );
						$tAntFromTask = model_antecedent::getInstance ()->findAllById ( $oTacheSimulee->id );
						foreach ( $tAntFromTask as $oAntTask ) {
							if ($oAntTask->IdTache == $i) {
								$antecedence [$oTache->id] [$i] = 1;
								break;
							} else {
								$antecedence [$oTache->id] [$i] = 0;
							}
						}
					} else {
						$antecedence [$oTache->id] [$i] = 0;
					}
				}
			}
			// calcul des etapes
			$etape = 0;
			do {
				if ($etape == 0) {
					for($tache = 1; $tache <= $nbTaches; $tache ++) {
						$scoreAnt [$tache] [$etape] = 0;
						for($j = 1; $j <= $nbTaches; $j ++) {
							$scoreAnt [$tache] [$etape] = $scoreAnt [$tache] [$etape] + $antecedence [$tache] [$j];
						}
						if ($scoreAnt [$tache] [$etape] == 0) {
							$phase [$etape] [] = $tache; // matrice de chronologie des taches par etapes
						}
					}
				} else {
					for($tache = 1; $tache <= $nbTaches; $tache ++) {
						if ($scoreAnt [$tache] [$etape - 1] != 0) {
							$scoreAnt [$tache] [$etape] = $scoreAnt [$tache] [$etape - 1];
		
							foreach ( $phase [$etape - 1] as $tachePasse ) { // verifier la valeur de tachePasse ->ok
								if ($antecedence [$tache] [$tachePasse] == 1) {
									$scoreAnt [$tache] [$etape] = $scoreAnt [$tache] [$etape] - 1;
								}
							}
							if ($scoreAnt [$tache] [$etape] == 0) {
								$phase [$etape] [] = $tache;
							}
						} else {
							$scoreAnt [$tache] [$etape] = 0;
						}
					}
				}
				$sommeScoreAnt = 0;
				for($i = 1; $i <= sizeof ( $scoreAnt ); $i ++) {
					$sommeScoreAnt = $sommeScoreAnt + $scoreAnt [$i] [$etape];
				}
				if ($etape == 0 && ! isset ( $phase )) {
					$error = array (
							'code' => 1,
							'message' => 'pas de tache sans antécédent.'
					);
				}
				$etape ++;
			} while ( ! ($sommeScoreAnt == 0 || isset ( $error )) );
		
			// calcul du pert
			for($iPhase = 0; $iPhase < sizeof ( $phase ); $iPhase ++) {
				foreach ( $phase [$iPhase] as $TachePhase ) {
					if ($iPhase == 0) {
						$oTacheSimulee = model_tachessimulees::getInstance ()->findByIdTache ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'], $TachePhase );
						$pert [$TachePhase] = array (
								'ID' => $TachePhase,
								'Duree' => $oTacheSimulee->DureeFinale,
								'DTO' => 0,
								'DTA' => 0,
								'MT' => 0,
								'ML' => 0,
								'FTO' => $oTacheSimulee->DureeFinale,
								'FTA' => 0
						);
					} else {
						$oTacheSimulee = model_tachessimulees::getInstance ()->findByIdTache ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'], $TachePhase );
						$tAntecdentTache = model_antecedent::getInstance ()->findAllById ( $oTacheSimulee->id );
						$dureeMax = 0;
						$idLongueTache = 0;
						foreach ( $tAntecdentTache as $oTacheAnt ) {
							if ($oTacheAnt->IdTache != 0 && $pert [$oTacheAnt->IdTache] ['FTO'] > $dureeMax) {
								$dureeMax = $pert [$oTacheAnt->IdTache] ['FTO'];
								$idLongueTache = $pert [$oTacheAnt->IdTache] ['ID'];
							}
						}
						// $oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi'],$TachePhase);
						$pert [$TachePhase] = array (
								'ID' => $TachePhase,
								'Duree' => $oTacheSimulee->DureeFinale,
								'DTO' => $pert [$idLongueTache] ['FTO'] + 1,
								'DTA' => 0,
								'MT' => 0,
								'ML' => 0,
								'FTO' => $pert [$idLongueTache] ['FTO'] + 1 + $oTacheSimulee->DureeFinale,
								'FTA' => 0
						);
					}
				}
			}
			// calcul de la dureeé total du projet
			$idDerniereTache = 0;
			$finProjet = 0;
			for($tache = 1; $tache < sizeof ( $pert ); $tache ++) {
				if ($pert [$tache] ['FTO'] > $finProjet) {
					$finProjet = $pert [$tache] ['FTO'];
					$idDerniereTache = $pert [$tache] ['ID'];
				}
			}
			$oResultat->delai_tot=$finProjet;
			
			// calcul du score 
			$oPenalite=model_penalite::getInstance()->findBySimu($_SESSION ['simulation']);
			$oSimulation=model_simulation::getInstance()->findById($_SESSION ['simulation']);
			$score=$oResultat->cout_tot;
			if ($oResultat->cout_tot > $oSimulation->budget){
				$score=$score + ($oResultat->cout_tot - $oSimulation->budget)*$oPenalite->budget;
			}
			if ($oResultat->delai_tot > $oSimulation->delai){
				$score=$score + ($oResultat->delai_tot - $oSimulation->delai)*$oPenalite->delai;
			}
			if ($oResultat->qualite_tot > $oSimulation->qualite){
				$score=$score + ($oResultat->qualite_tot - $oSimulation->qualite)*$oPenalite->qualite;
			}
			$oTablette=model_choixcomposant::getInstance()->findByContext($_SESSION['nEquipe'],$_SESSION['simulation'],$_SESSION['suivi']);
			$oClavier=model_composant::getInstance()->findById($oTablette->id_clavier); 
			if($oClavier->poids == 0){
				if ($oResultat->poids > 825){
					$score=$score + $oPenalite->poids;
				}
			}else {
				if ($oResultat->poids > 1155){
					$score=$score + $oPenalite->poids;
				}
			}
			$oBatterie=model_composant::getInstance()->findById($oTablette->id_battrie);
			if ($oResultat->puissance > $oBatterie->puissance){
				$score=$score + $oPenalite->battrie;
			}
			
			$oResultat->score=$score;
		
			// enregistrement des resultats
			if ($oResultat->save ()) {
				_root::redirect('tachessimulees::detail',array('suivi'=>$_SESSION['suivi']));
			} else {
				return $oResultat->getListError ();
			}
			
			
			
		} else {
			return $oValidation->getListError ();
		}
	}
	public function after() {
		$this->oLayout->show ();
	}
}

