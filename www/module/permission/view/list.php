<table class="table table-striped">
	<tr>
		
		<th>action</th>

		<th>element</th>

		<th>allowdeny</th>

		<th>groupe</th>

		<th></th>
	</tr>
	<?php if($this->tPermission):?>
		<?php foreach($this->tPermission as $oPermission):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oPermission->action ?></td>

		<td><?php echo $oPermission->element ?></td>

		<td><?php echo $oPermission->allowdeny ?></td>

		<td><?php if(isset($this->tJoinmodel_groupe[$oPermission->groupe_id])){ echo $this->tJoinmodel_groupe[$oPermission->groupe_id];}else{ echo $oPermission->groupe_id ;}?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('Permission::edit',array(
										'id'=>$oPermission->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('Permission::delete',array(
										'id'=>$oPermission->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="5">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('Permission::new') ?>">New</a></p>

