<table class="table table-striped">
	<tr>
		
		<th>Limite</th>

		<th></th>
	</tr>
	<?php if($this->tVariationsqualite):?>
		<?php foreach($this->tVariationsqualite as $oVariationsqualite):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oVariationsqualite->Limite ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('variationsqualite::edit',array(
										'id'=>$oVariationsqualite->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('variationsqualite::delete',array(
										'id'=>$oVariationsqualite->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="2">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('variationsqualite::new') ?>">New</a></p>

