<?php 
class module_variationsqualite extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tVariationsqualite=model_variationsqualite::getInstance()->findAll();
		
		$oView=new _view('variationsqualite::list');
		$oView->tVariationsqualite=$tVariationsqualite;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oVariationsqualite=new row_variationsqualite;
		
		$oView=new _view('variationsqualite::new');
		$oView->oVariationsqualite=$oVariationsqualite;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oVariationsqualite=model_variationsqualite::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('variationsqualite::edit');
		$oView->oVariationsqualite=$oVariationsqualite;
		$oView->tId=model_variationsqualite::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oVariationsqualite=model_variationsqualite::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('variationsqualite::delete');
		$oView->oVariationsqualite=$oVariationsqualite;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oVariationsqualite=new row_variationsqualite;	
		}else{
			$oVariationsqualite=model_variationsqualite::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('Limite');
		foreach($tColumn as $sColumn){
			$oVariationsqualite->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oVariationsqualite->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('variationsqualite::list');
		}else{
			return $oVariationsqualite->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oVariationsqualite=model_variationsqualite::getInstance()->findById( _root::getParam('id',null) );
				
		$oVariationsqualite->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('variationsqualite::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

