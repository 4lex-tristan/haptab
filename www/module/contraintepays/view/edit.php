<?php 
$oForm=new plugin_form($this->oContraintepays);
$oForm->setMessage($this->tMessage);
?>
<form class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Pays</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('Pays',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('IdSimu',$this->tJoinmodel_simulation,array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb Stagiaires</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NbStag',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb Ouvriers</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('NbOuvrier',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heures Hebdomadaire</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('HeureHebdomadaire',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Jours Hebdomadaire</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('JourHebdomadaire',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Max Heures Sup.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('MaxHeureSup',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Ing.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('CoutHoraireIng',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Tech.</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('CoutHoraireTech',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Ouvrier</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('CoutHoraireOuvrier',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Conversion USD - local</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('ConversionDollarLocal',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('contraintepays::list')?>">Annuler</a>
	</div>
</div>
</form>

