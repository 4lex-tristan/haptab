<table class="table table-striped">
	<tr>
		
		<th>Pays</th>

		<th>Nb Stagiaires</th>

		<th>Nb Ouvriers</th>

		<th>Heures Hebdomadaire</th>

		<th>Jours Hebdomadaire</th>

		<th>Max Heures Sup.</th>

		<th>Cout Horaire Ing.</th>

		<th>Cout Horaire Tech.</th>

		<th>Cout Horaire Ouvrier</th>

		<th>Conversion USD - local</th>

		<th></th>
	</tr>
	<?php if($this->tContraintepays):?>
		<?php foreach($this->tContraintepays as $oContraintepays):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oContraintepays->Pays ?></td>

		<td><?php echo $oContraintepays->NbStag ?></td>

		<td><?php echo $oContraintepays->NbOuvrier ?></td>

		<td><?php echo $oContraintepays->HeureHebdomadaire ?></td>

		<td><?php echo $oContraintepays->JourHebdomadaire ?></td>

		<td><?php echo $oContraintepays->MaxHeureSup ?></td>

		<td><?php echo $oContraintepays->CoutHoraireIng ?></td>

		<td><?php echo $oContraintepays->CoutHoraireTech ?></td>

		<td><?php echo $oContraintepays->CoutHoraireOuvrier ?></td>

		<td><?php echo $oContraintepays->ConversionDollarLocal ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('contraintepays::edit',array(
										'id'=>$oContraintepays->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('contraintepays::delete',array(
										'id'=>$oContraintepays->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="11">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('contraintepays::new') ?>">New</a></p>

