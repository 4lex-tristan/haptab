<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">Pays</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->Pays ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb Stagiaires</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->NbStag ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Nb Ouvriers</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->NbOuvrier ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Heures Hebdomadaire</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->HeureHebdomadaire ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Jours Hebdomadaire</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->JourHebdomadaire ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Max Heures Sup.</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->MaxHeureSup ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Ing.</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->CoutHoraireIng ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Tech.</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->CoutHoraireTech ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Cout Horaire Ouvrier</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->CoutHoraireOuvrier ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Conversion USD - local</label>
		<div class="col-sm-10"><?php echo $this->oContraintepays->ConversionDollarLocal ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('contraintepays::list')?>">Annuler</a>
	</div>
</div>
</form>
