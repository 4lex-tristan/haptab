<?php 
class module_contraintepays extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
		
		//$this->oLayout->addModule('menu','menu::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tContraintepays=model_contraintepays::getInstance()->findAll();
		
		$oView=new _view('contraintepays::list');
		$oView->tContraintepays=$tContraintepays;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oContraintepays=new row_contraintepays;
		
		$oView=new _view('contraintepays::new');
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->oContraintepays=$oContraintepays;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oContraintepays=model_contraintepays::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('contraintepays::edit');
		$oView->oContraintepays=$oContraintepays;
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		$oView->tId=model_contraintepays::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oContraintepays=model_contraintepays::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('contraintepays::delete');
		$oView->oContraintepays=$oContraintepays;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oContraintepays=new row_contraintepays;	
		}else{
			$oContraintepays=model_contraintepays::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('Pays','IdSimu','NbStag','NbOuvrier','HeureHebdomadaire','JourHebdomadaire','MaxHeureSup','CoutHoraireIng','CoutHoraireTech','CoutHoraireOuvrier','ConversionDollarLocal');
		foreach($tColumn as $sColumn){
			$oContraintepays->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oContraintepays->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('contraintepays::list');
		}else{
			return $oContraintepays->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oContraintepays=model_contraintepays::getInstance()->findById( _root::getParam('id',null) );
				
		$oContraintepays->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('contraintepays::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

