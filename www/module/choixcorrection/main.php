<?php 
class module_choixcorrection extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		$this->oLayout=new _layout('bootstrap');

		$this->oLayout->addModule('menu','menuSimulation::index');
	}
	
	
	public function _index(){
			if (model_parametrevalidation::getInstance ()->eventIsValidated ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				$oTablette = model_choixcorrections::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
				_root::redirect ( 'choixcorrection::detail&id=' . $oTablette->id );
			} elseif (model_choixcorrections::getInstance ()->isFulfilled ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) == true) {
				$oTablette = model_choixcorrections::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
				_root::redirect ( 'choixcorrection::validate&id=' . $oTablette->id );
			} else {
				$this->_new ();
			}
	}
	
	
	public function _validate(){
		$tMessage=$this->ProcessValidation();
		$oChoixcorrection=model_choixcorrections::getInstance()->findById( _root::getParam('id') );
		
		$oEvent=model_evenements::getInstance()->findByContext($_SESSION['simulation'],$_SESSION['suivi']);
		$oCorrection=model_correctionsevent::getInstance()->findById($oChoixcorrection->id_correction);
		
		$oView=new _view('choixcorrection::validate');
		$oView->oEvent=$oEvent;
		$oView->oCorrection=$oCorrection;
		$oView->oChoixcorrection=$oChoixcorrection;
			
			
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new() {
		if ($_SESSION ['suivi'] == - 1) { // si la date correspondat a une date de suivi et n'est pas entre 2 suivis
			$oView = new _view ( 'choixcorrection::nosuivi' );
		} else {
			
			if (model_evenements::getInstance()->findByContext($_SESSION['simulation'],$_SESSION['suivi'])){
				$tMessage = $this->processSave ();
				
				$oEvent=model_evenements::getInstance()->findByContext($_SESSION['simulation'],$_SESSION['suivi']);
				$tCorrections=model_correctionsevent::getInstance()->findAllByEvent($oEvent->id);
				$oChoixcorrection = new row_choixcorrections();
					
				$oView = new _view ( 'choixcorrection::new' );
				$oView->oEvent=$oEvent;
				$oView->tCorrections=$tCorrections;
				$oView->oChoixcorrection=$oChoixcorrection;
				$oView->tJoinmodel_corrections=model_correctionsevent::getInstance()->getselect();
					
				$oPluginXsrf = new plugin_xsrf ();
				$oView->token = $oPluginXsrf->getToken ();
				$oView->tMessage = $tMessage;
			}else {
				$oView = new _view ( 'choixcorrection::noevent' );
			}
			
		}
		
		$this->oLayout->add ( 'main', $oView );
	}

	
	
	public function _edit(){
		if ($_SESSION ['suivi'] == - 1) { // si la date correspondat a une date de suivi et n'est pas entre 2 suivis
			$oView = new _view ( 'choixcorrection::nosuivi' );
		} else {
		$tMessage=$this->processSave();
		
		$oEvent=model_evenements::getInstance()->findByContext($_SESSION['simulation'],$_SESSION['suivi']);
		$tCorrections=model_correctionsevent::getInstance()->findAllByEvent($oEvent->id);
		$oChoixcorrection=model_choixcorrections::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('choixcorrection::edit');
		$oView->oEvent=$oEvent;
		$oView->tCorrections=$tCorrections;
		$oView->oChoixcorrection=$oChoixcorrection;
		$oView->tJoinmodel_corrections=model_correctionsevent::getInstance()->getselect();
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		}
		
		$this->oLayout->add('main',$oView);
	}

	public function _detail() {
		if (_root::getParam ( 'id' )){
			$oChoixcorrection = model_choixcorrections::getInstance ()->findById ( _root::getParam ( 'id' ) );
			$oEvent=model_evenements::getInstance()->findByContext($_SESSION['simulation'],$_SESSION['suivi']);
		}elseif (_root::getParam('suivi')){
			$oChoixcorrection = model_choixcorrections::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'],_root::getParam ( 'suivi' ) );
			$oEvent=model_evenements::getInstance()->findByContext($_SESSION['simulation'],_root::getParam ( 'suivi' ));
		}
		
		
		$oCorrection=model_correctionsevent::getInstance()->findById($oChoixcorrection->id_correction);
		
		$oView = new _view ( 'choixcorrection::detail' );
		$oView->oChoixcorrection = $oChoixcorrection;
		$oView->oEvent=$oEvent;
		$oView->oCorrection=$oCorrection;
		
		$this->oLayout->add ( 'main', $oView );
	}
	
	
	

	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oChoixcorrection=new row_choixcorrections;	
		}else{
			$oChoixcorrection=model_choixcorrections::getInstance()->findById( _root::getParam('id',null) );
		}
		$oChoixcorrection->id_equipe=$_SESSION['nEquipe'];
		$oChoixcorrection->id_simu=$_SESSION['simulation'];
		$oChoixcorrection->id_suivi=$_SESSION['suivi'];
		$tColumn=array('id_event','id_correction');
		foreach($tColumn as $sColumn){
			$oChoixcorrection->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oChoixcorrection->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('choixcorrection::index');
		}else{
			return $oChoixcorrection->getListError();
		}
		
	}
	
	private function ProcessValidation() {
		if (! _root::getRequest ()->isPost ()) { // si ce n'est pas une requete POST on ne valide pas
			return null;
		}
		
		$oPluginXsrf = new plugin_xsrf ();
		if (! $oPluginXsrf->checkToken ( _root::getParam ( 'token' ) )) { // on verifie que le token est valide
			return array (
					'token' => $oPluginXsrf->getMessage () 
			);
		}
		
		if (model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] ) != null) {
			$oValidation = model_parametrevalidation::getInstance ()->findByContext ( $_SESSION ['nEquipe'], $_SESSION ['simulation'], $_SESSION ['suivi'] );
		} else {
			$oValidation = new row_parametrevalidation ();
		}
		
		$oValidation->id_equipe = $_SESSION ['nEquipe'];
		$oValidation->id_simu = $_SESSION ['simulation'];
		$oValidation->suivi = $_SESSION ['suivi'];
		$oValidation->event_valid = 1;
		
		if ($oValidation->save ()) {
			_root::redirect('choixcorrection::index');
		} else {
			return $oValidation->getListError ();
		}
	}

	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

