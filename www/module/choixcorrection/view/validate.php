<?php 
$oForm=new plugin_form();
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	<div class="form-group">
		<label class="col-sm-2 control-label">Evènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->nom?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10"><?php echo $this->oEvent->description?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Mesure Corrective Sélectionnée</label>
		<div class="col-sm-10">
		<table class="table">
			<tr>
				<th>Intitulé</th>
				<th>Impact qualite</th>
				<th>Impact coût</th>
				<th>Impact délai</th>
			</tr>
			<tr>
				<td><?php echo $this->oCorrection->nom ?></td>
				<td><?php echo $this->oCorrection->qualite ?></td>
				<td><?php echo $this->oCorrection->cout ?></td>
				<td><?php echo $this->oCorrection->delai ?></td>
			</tr>
		</table>
		</div>
	</div>
	
	
	<?php echo $oForm->getToken('token',$this->token)?>
	<input type="hidden" name="validate" value="True" />
	<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<input type="submit" class="btn btn-success">
		<a class="btn btn-warning" href="<?php echo $this->getLink('choixcorrection::edit',array('id'=>$this->oChoixcorrection->id))?>">Modifier</a>
	</div>
</div>
</form>