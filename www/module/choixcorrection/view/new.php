<?php 
$oForm=new plugin_form($this->oChoixcorrection);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">Evènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->nom?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10"><?php echo $this->oEvent->description?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Mesures Correctives</label>
		<div class="col-sm-10">
		<table class="table">
			<tr>
				<th>Intitulé</th>
				<th>Impact qualite</th>
				<th>Impact coût</th>
				<th>Impact délai</th>
			</tr>
			<?php foreach ($this->tCorrections as $oCorrections):?>
			<tr>
				<td><?php echo $oCorrections->nom ?></td>
				<td><?php echo $oCorrections->qualite ?></td>
				<td><?php echo $oCorrections->cout ?></td>
				<td><?php echo $oCorrections->delai ?></td>
			</tr>
			<?php endforeach;?>
		</table>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Choix de mesure de corrections</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('id_correction',$this->tJoinmodel_corrections,array('class'=>'form-control'))?></div>
	</div>

	<?php echo '<input type="hidden" name="id_event" value="'.$this->oEvent->id.'  ">' ?>

<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Soumettre" />
	</div>
</div>
</form>
