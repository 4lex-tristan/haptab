<form  class="form-horizontal" action="" method="POST" >

	<div class="form-group">
		<label class="col-sm-2 control-label">Evènement</label>
		<div class="col-sm-10"><?php echo $this->oEvent->nom?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10"><?php echo $this->oEvent->description?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Mesure Corrective Sélectionnée</label>
		<div class="col-sm-10">
		<table class="table">
			<tr>
				<th>Intitulé</th>
				<th>Impact qualite</th>
				<th>Impact coût</th>
				<th>Impact délai</th>
			</tr>
			<tr>
				<td><?php echo $this->oCorrection->nom ?></td>
				<td><?php echo $this->oCorrection->qualite ?></td>
				<td><?php echo $this->oCorrection->cout ?></td>
				<td><?php echo $this->oCorrection->delai ?></td>
			</tr>
		</table>
		</div>
	</div>
	
	<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<a class="btn btn-link" href="<?php echo $this->getLink('tdbGroupe::index')?>">Retour</a>
	</div>
</div>
</form>