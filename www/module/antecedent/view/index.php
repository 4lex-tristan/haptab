<?php
echo 'duree total du projet'.$this->dureeProjet; ?>
<?php if (isset($this->error)):?>
<p>!! <?php echo $this->error['message']?></p>
<?php endif;?>
<table class="table table-striped">
	<tr>
		<th></th>
	<?php for($i=1;$i<=sizeof($this->tAnt);$i++):?>
	<th><?php echo $i?></th>
	<?php endfor;?>
	
	<?php if (!isset($this->error)):?>
		<?php for($etape=0;$etape<sizeof($this->tScore[1]);$etape++):?>
		<th><?php echo $etape?></th>
		<?php endfor;?>
	<?php endif;?>
	</tr>
	<?php  for($i=1;$i<=sizeof($this->tAnt);$i++):?>
		<tr>
		<th><?php echo $i?></th>
		<?php for($j=1;$j<=sizeof($this->tAnt);$j++):?>
		<td><?php echo $this->tAnt[$i][$j]?></td>
		<?php endfor;?>
		
		<?php if (!isset($this->error)):?>
			<?php for($phase=0;$phase<sizeof($this->tScore[1]);$phase++):?>
			<td><?php echo $this->tScore[$i][$phase]?></td>
			<?php endfor;?>
		<?php endif;?>
		</tr>
	<?php endfor;?>
</table>

<?php if (!isset($this->error)):?>
taches par phases
<table class="table table-striped">
	<tr>
		<th>phases</th>
		<?php for($i=0;$i<sizeof($this->tTachesParPhase);$i++):?>
		<th><?php echo $i?></th>
		<?php endfor;?>
	</tr>
	<tr>
		<th>taches</th>
		<?php for($i=0;$i<sizeof($this->tTachesParPhase);$i++):?>
			<td><?php	
			for($j = 0; $j < sizeof ( $this->tTachesParPhase [$i] ); $j ++) {
					echo $this->tTachesParPhase [$i] [$j] . '</br>';
				}?>
			</td>
		<?php endfor;?>
	</tr>
</table>
<?php endif;?>

<?php if (!isset($this->error)):?>
pert
<table class="table table-striped">
	<tr>
		<th>Taches</th>
		<th>Durée</th>
		<th>DTO</th>
		<th>DTA</th>
		<th>MT</th>
		<th>ML</th>
		<th>FTO</th>
		<th>FTA</th>
	</tr>
	<?php for($i=1;$i<=sizeof($this->tPert);$i++):?>
	<tr>
		<td><?php	echo $this->tPert[$i]['ID']?></td>
		<td><?php echo $this->tPert[$i]['Duree']?></td>
		<td><?php echo $this->tPert[$i]['DTO']?></td>
		<td><?php echo $this->tPert[$i]['DTA']?></td>
		<td><?php echo $this->tPert[$i]['MT']?></td>
		<td><?php echo $this->tPert[$i]['ML']?></td>
		<td><?php echo $this->tPert[$i]['FTO']?></td>
		<td><?php echo $this->tPert[$i]['FTA']?></td>
	</tr>
	<?php endfor;?>
</table>
<?php endif;?>
