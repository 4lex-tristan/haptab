<?php 
class module_antecedent extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	/* #debutaction#
	public function _exampleaction(){
	
		$oView=new _view('examplemodule::exampleaction');
		
		$this->oLayout->add('main',$oView);
	}
	#finaction# */
	
	
	public function _index(){
		$oAccount=model_account::getInstance()->findById(_root::getParam('id'));
		$tTaches=model_taches::getInstance()->findAllBySim($oAccount->id_simulation);
		// calcul de la table des antecedence 
		$nbTaches= sizeof($tTaches);
		foreach ($tTaches as $oTache){
			for ($i=1;$i<=$nbTaches;$i++){
				if ($oTache->id != $i){
					$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$oTache->id);
					$tAntFromTask=model_antecedent::getInstance()->findAllById($oTacheSimulee->id);
					foreach ($tAntFromTask as $oAntTask){
						if ($oAntTask->IdTache == $i){
							$antecedence[$oTache->id][$i] = 1;
							break;
						}else {
							$antecedence[$oTache->id][$i] = 0;
						}
					}
				}else {
					$antecedence[$oTache->id][$i] = 0;
				}
			}
			
		}
		//calcul des etapes 
		$etape = 0;
		do {
			if ($etape == 0){
				for ($tache=1;$tache<=$nbTaches;$tache++){
					$scoreAnt[$tache][$etape]=0;
					for ($j=1;$j<=$nbTaches;$j++){
						$scoreAnt[$tache][$etape]=$scoreAnt[$tache][$etape]+$antecedence[$tache][$j];
					}
					if ($scoreAnt[$tache][$etape] ==0){
						$phase[$etape][]=$tache; //matrice de chronologie des taches par etapes
					}
				}
			}else {
				for ($tache=1;$tache<=$nbTaches;$tache++){
					if ($scoreAnt[$tache][$etape-1] !=0){
						$scoreAnt[$tache][$etape]=$scoreAnt[$tache][$etape-1];
						//if (isset($phase)){
							foreach ($phase[$etape-1] as $tachePasse){ // verifier la valeur de tachePasse
								if ($antecedence[$tache][$tachePasse] == 1){
									$scoreAnt[$tache][$etape] = $scoreAnt[$tache][$etape] -1;
								}
							}
						//}
						if ($scoreAnt[$tache][$etape] == 0 ){
							$phase[$etape][]=$tache;
						}
					}else {
					$scoreAnt[$tache][$etape]=0;
					}
					
				}
			}
			$sommeScoreAnt=0;
			for($i=1;$i<=sizeof($scoreAnt);$i++){
				$sommeScoreAnt=$sommeScoreAnt+$scoreAnt[$i][$etape];
			}
			if ($etape == 0 && ! isset($phase) ){
				$error=array('code'=>1,'message'=>'pas de tache sans antécédent.');
			}
			$etape++;
			
			
		}while (!($sommeScoreAnt ==0 ||  isset($error)) );

		
		//calcul du pert
		for($iPhase=0;$iPhase<sizeof($phase);$iPhase++){
			foreach ($phase[$iPhase] as $TachePhase){
				if($iPhase == 0){
					$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$TachePhase);
					$pert[$TachePhase]=array('ID'=> $TachePhase,'Duree'=>$oTacheSimulee->DureeFinale,'DTO'=> 0,'DTA'=> 0, 'MT'=> 0,'ML'=> 0,'FTO'=> $oTacheSimulee->DureeFinale,'FTA'=>0);
				}else{
					$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$TachePhase);
					$tAntecdentTache=model_antecedent::getInstance()->findAllById($oTacheSimulee->id);
					$dureeMax=0;
					$idLongueTache=0;
					foreach ($tAntecdentTache as $oTacheAnt){
						if($oTacheAnt->IdTache !=0 && $pert[$oTacheAnt->IdTache]['FTO'] > $dureeMax){
							$dureeMax=$pert[$oTacheAnt->IdTache]['FTO'];
							$idLongueTache=$pert[$oTacheAnt->IdTache]['ID'];
						}
					}
					//$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$TachePhase);
					$pert[$TachePhase]=array('ID'=> $TachePhase,'Duree'=>$oTacheSimulee->DureeFinale, 'DTO'=> $pert[$idLongueTache]['FTO']+1,'DTA'=> 0, 'MT'=> 0,'ML'=> 0,'FTO'=> $pert[$idLongueTache]['FTO']+1+$oTacheSimulee->DureeFinale,'FTA'=>0);
				}
			}
		}
		// calcul de la dureeé total du projet
		$idDerniereTache=0;
		$finProjet=0;
		for($tache=1;$tache<sizeof($pert);$tache++){
			if ($pert[$tache]['FTO']> $finProjet){
				$finProjet = $pert[$tache]['FTO'];
				$idDerniereTache = $pert[$tache]['ID'];
			}
		}
		
		//$pert[30]['FTA']=$pert[30]['FTO'];
		//$pert[30]['DTA']=$pert[30]['FTO']-$pert[30]['Duree'];
		
		/* calcul du FTA et DTA 
		$nbPhase=sizeof($phase)-1;
		for($iPhase=$nbPhase;$iPhase>=0;$iPhase--){
			foreach ($phase[$iPhase] as $TachePhase){
				if ($iPhase == $nbPhase){
					$pert[$TachePhase]['FTA']=$finProjet;
					//$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$TachePhase);
					$pert[$TachePhase]['DTA']=$pert[$TachePhase]['FTA']-$pert[$TachePhase]['Duree']; //$oTacheSimulee->DureeFinale
				}else{
					$tPost=model_antecedent::getInstance()->findAllByPostTask($TachePhase);
					$lowestDTA=10000;
					$idPost=0;
					foreach ($tPost as $oPost){
						if($oTacheSimulee=model_tachessimulees::getInstance()->findById($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$oPost->IdTacheSimule)){
							if ($pert[$oPost->IdTacheSimule]['DTA'] < $lowestDTA){
								$lowestDTA = $pert[$oPost->IdTacheSimule]['DTA'];
								$idPost=$oPost->IdTacheSimule;
							}
						}
						
					}
					if ($idPost != 0){
						$pert[$TachePhase]['FTA']=$pert[$idPost]['DTA'] - 1;
					}else{
						$pert[$TachePhase]['FTA']=$finProjet;
					}
					//$oTacheSimulee=model_tachessimulees::getInstance()->findByIdTache($oAccount->id,$oAccount->id_simulation,_root::getParam('suivi'),$TachePhase);
					$pert[$TachePhase]['DTA']=$pert[$TachePhase]['FTA']-$pert[$TachePhase]['Duree'];
				}
			}
		}
		
		// clacul du ML et MT 
		for($tache=1;$tache<sizeof($pert);$tache++){
			$pert[$tache]['ML']= $pert[$tache]['FTO']-$pert[$tache]['DTO']-$pert[$tache]['Duree'];
			$pert[$tache]['MT']= $pert[$tache]['FTA']-$pert[$tache]['DTO']-$pert[$tache]['Duree'];
		}
		*/
	
		
		
		$oView=new _view('antecedent::index');
		$oView->tAnt=$antecedence;
		$oView->tScore=$scoreAnt;
		if (!isset($error) || ( isset($error) && $error['code'] != 1 )){
			$oView->tTachesParPhase=$phase;
			$oView->tPert=$pert;
			$oView->dureeProjet=$finProjet;
		}else{
			$oView->error=$error;
		}
		$this->oLayout->add('main',$oView);
	}
	
	
	public function after(){
		$this->oLayout->show();
	}
	
	
}
