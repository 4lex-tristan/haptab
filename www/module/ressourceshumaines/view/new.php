<?php 
$oForm=new plugin_form($this->oRessourceshumaines);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">nom</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('nom',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">pays</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('pays',$this->tJoinmodel_contraintepays,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">metier</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('metier', array('Ingenieur'=> 'Ingenieur','Technicien'=>'Technicien'),array('class'=>'form-control'))//$oForm->getInputText('metier',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau_competence</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('niveau_competence',array('1','2','3','4','5'),array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau_rapidite</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('niveau_rapidite',array('1','2','3','4','5'),array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">prime</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('prime',array('class'=>'form-control'))?></div>
	</div>

	<!-- <div class="form-group">
		<label class="col-sm-2 control-label">competence</label>
		<div class="col-sm-10"><?php //echo $oForm->getInputText('competence',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">rapidite</label>
		<div class="col-sm-10"><?php //echo $oForm->getInputText('rapidite',array('class'=>'form-control'))?></div>
	</div> -->


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Ajouter" /> <a class="btn btn-link" href="<?php echo $this->getLink('ressourceshumaines::list')?>">Annuler</a>
	</div>
</div>
</form>
