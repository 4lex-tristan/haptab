<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">nom</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">pays</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_contraintepays[$this->oRessourceshumaines->pays]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">metier</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->metier ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau_competence</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->niveau_competence ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau_rapidite</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->niveau_rapidite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">prime</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->prime ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">competence</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->competence ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">rapidite</label>
		<div class="col-sm-10"><?php echo $this->oRessourceshumaines->rapidite ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('ressourceshumaines::list')?>">Annuler</a>
	</div>
</div>
</form>
