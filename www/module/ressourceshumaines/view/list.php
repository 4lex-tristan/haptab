<table class="table table-striped">
	<tr>
		
		<th>nom</th>

		<th>pays</th>

		<th>metier</th>

		<th>niveau_competence</th>

		<th>niveau_rapidite</th>

		<th>prime</th>

		<th>competence</th>

		<th>rapidite</th>

		<th></th>
	</tr>
	<?php if($this->tRessourceshumaines):?>
		<?php foreach($this->tRessourceshumaines as $oRessourceshumaines):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oRessourceshumaines->nom ?></td>

		<td><?php if(isset($this->tJoinmodel_contraintepays[$oRessourceshumaines->id_pays])){ echo $this->tJoinmodel_contraintepays[$oRessourceshumaines->id_pays];}else{ echo $oRessourceshumaines->id_pays ;}?></td>

		<td><?php echo $oRessourceshumaines->metier ?></td>

		<td><?php echo $oRessourceshumaines->niveau_competence ?></td>

		<td><?php echo $oRessourceshumaines->niveau_rapidite ?></td>

		<td><?php echo $oRessourceshumaines->prime ?></td>

		<td><?php echo $oRessourceshumaines->competence ?></td>

		<td><?php echo $oRessourceshumaines->rapidite ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('ressourceshumaines::edit',array(
										'id'=>$oRessourceshumaines->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('ressourceshumaines::delete',array(
										'id'=>$oRessourceshumaines->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="9">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('ressourceshumaines::new') ?>">New</a>  <a class="btn btn-danger" href="<?php echo $this->getLink('ressourceshumaines::vargen')?>">Calcul Comp. et Rapidité</a></p>

