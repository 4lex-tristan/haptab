<?php 
class module_ressourceshumaines extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
		//$this->oLayout->addModule('menu','menu::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tRessourceshumaines=model_ressourceshumaines::getInstance()->findAll();
		
		$oView=new _view('ressourceshumaines::list');
		$oView->tRessourceshumaines=$tRessourceshumaines;
		
		$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oRessourceshumaines=new row_ressourceshumaines;
		
		$oView=new _view('ressourceshumaines::new');
		$oView->oRessourceshumaines=$oRessourceshumaines;
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oRessourceshumaines=model_ressourceshumaines::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('ressourceshumaines::edit');
		$oView->oRessourceshumaines=$oRessourceshumaines;
		$oView->tId=model_ressourceshumaines::getInstance()->getIdTab();
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oRessourceshumaines=model_ressourceshumaines::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('ressourceshumaines::delete');
		$oView->oRessourceshumaines=$oRessourceshumaines;
		
				$oView->tJoinmodel_contraintepays=model_contraintepays::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}
	
	public function _vargen(){
		$tMessage=$this->variationsGen();
		
		$oView=new _view('ressourceshumaines::vargen');
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oRessourceshumaines=new row_ressourceshumaines;
			// calcul Competence
			switch (_root::getParam('niveau_competence',null)){
				case 1:
					$oRessourceshumaines->competence=rand(70,90);
					break;
				case 2:
					$oRessourceshumaines->competence=rand(80,100);
					break;
				case 3:
					$oRessourceshumaines->competence=rand(90,110);
					break;
				case 4:
					$oRessourceshumaines->competence=rand(100,120);
					break;
				case 5:
					$oRessourceshumaines->competence=rand(110,130);
					break;
			} 
			
			// calcul rapidité
			switch (_root::getParam('niveau_rapidite',null)){
				case 1:
					$oRessourceshumaines->rapidite=rand(70,90);
					break;
				case 2:
					$oRessourceshumaines->rapidite=rand(80,100);
					break;
				case 3:
					$oRessourceshumaines->rapidite=rand(90,110);
					break;
				case 4:
					$oRessourceshumaines->rapidite=rand(100,120);
					break;
				case 5:
					$oRessourceshumaines->rapidite=rand(110,130);
					break;
			}
			$tColumn=array('nom','pays','metier','niveau_competence','niveau_rapidite','prime');
			foreach($tColumn as $sColumn){
				$oRessourceshumaines->$sColumn=_root::getParam($sColumn,null) ;
			}
		}else{
			$oRessourceshumaines=model_ressourceshumaines::getInstance()->findById( _root::getParam('id',null) );
			$tColumn=array('nom','pays','metier','niveau_competence','niveau_rapidite','prime','competence','rapidite');
			foreach($tColumn as $sColumn){
				$oRessourceshumaines->$sColumn=_root::getParam($sColumn,null) ;
			}
		}
		
		
		if($oRessourceshumaines->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('ressourceshumaines::list');
		}else{
			return $oRessourceshumaines->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oRessourceshumaines=model_ressourceshumaines::getInstance()->findById( _root::getParam('id',null) );
				
		$oRessourceshumaines->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('ressourceshumaines::list');
		
	}
	
	public function variationsGen() {
		if (! _root::getRequest ()->isPost ()) { // si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf = new plugin_xsrf ();
		if (! $oPluginXsrf->checkToken ( _root::getParam ( 'token' ) )) { // on verifie que le token est valide
			return array (
					'token' => $oPluginXsrf->getMessage () 
			);
		}
		$tRessourcesHumaines = model_ressourceshumaines::getInstance ()->findAll ();
		foreach ( $tRessourcesHumaines as $oRH ) {
			// calcul Competence
			switch ($oRH->niveau_competence) {
				case 1 :
					$oRH->competence = rand ( 70, 90 );
					break;
				case 2 :
					$oRH->competence = rand ( 80, 100 );
					break;
				case 3 :
					$oRH->competence = rand ( 90, 110 );
					break;
				case 4 :
					$oRH->competence = rand ( 100, 120 );
					break;
				case 5 :
					$oRH->competence = rand ( 110, 130 );
					break;
			}
			
			// calcul rapidité
			switch ($oRH->niveau_rapidite) {
				case 1 :
					$oRH->rapidite = rand ( 70, 90 );
					break;
				case 2 :
					$oRH->rapidite = rand ( 80, 100 );
					break;
				case 3 :
					$oRH->rapidite = rand ( 90, 110 );
					break;
				case 4 :
					$oRH->rapidite = rand ( 100, 120 );
					break;
				case 5 :
					$oRH->rapidite = rand ( 110, 130 );
					break;
			}
			if ($oRH->save ()) {
			} else {
				return $oRH->getListError ();
			}
		}
		// une fois les variations generées on redirige (vers la page liste)
		_root::redirect ( 'ressourceshumaines::list' );
	}

	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

