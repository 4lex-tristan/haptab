<?php 
class module_tdbAdmin extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	/* #debutaction#
	public function _exampleaction(){
	
		$oView=new _view('examplemodule::exampleaction');
		
		$this->oLayout->add('main',$oView);
	}
	#finaction# */
	
	
	public function _index(){
		$tSimu=model_simulation::getInstance()->findAll();
		foreach ($tSimu as $oSimu){
			$tGroupes[$oSimu->name]=model_account::getInstance()->findBySimuAndGroup($oSimu->id,2); // 2 represente la categorie des equipes 
			foreach ($tGroupes[$oSimu->name] as $oGroupe){
				$tSuivisSimu=model_datessuivi::getInstance()->findBySimu($oSimu->id);
				$nbSuivi=0;
				$validated=0;
				foreach ($tSuivisSimu as $oSuivi){
					$oValidation=model_parametrevalidation::getInstance()->findByContext($oGroupe->id,$oSimu->id,$oSuivi->id);
					if(isset($oValidation)){
					if ($oValidation->tablette_valid ==1){
						$validated=$validated+1;
					}
					if($oValidation->tache_valid ==1){
						$validated=$validated+1;
					}
					if($oValidation->risque_valid==1){
						$validated=$validated+1;
					}
					}
					$oDateDebut = new plugin_date ( $oSuivi->dateDebut );
					$oDateFin = new plugin_date ( $oSuivi->dateFin );
					if (time () > $oDateDebut->getMkTime () && time () < $oDateFin->getMkTime ()) {
						$suiviActuel= $nbSuivi;
					}
					$nbSuivi++;
				}
				if(!isset($suiviActuel)){
					$suiviActuel=$nbSuivi;
				}
				if (isset($nbSuivi) and $nbSuivi != 0 ){
					$tProgres[$oSimu->name][$oGroupe->login]['validated']=$validated/($nbSuivi*3)*100;
					$tProgres[$oSimu->name][$oGroupe->login]['progress']=($suiviActuel*3)/($nbSuivi*3)*100;
				}
				else{
					$tProgres[$oSimu->name][$oGroupe->login]['validated']=0;
					$tProgres[$oSimu->name][$oGroupe->login]['progress']=0;
				}
				
				/*$tProgres[$oSimu->name][$oGroupe->login]['validated']=5/(3*3)*100;
				$tProgres[$oSimu->name][$oGroupe->login]['progress']=(2*3)/(3*3)*100;*/
			}
		}
		
		
	
		$oView=new _view('tdbAdmin::index');
		$oView->tSimu=$tSimu;
		$oView->tGroupes=$tGroupes;
		$oView->tProgres=$tProgres;
		$this->oLayout->add('main',$oView);
	}
	
	
	public function after(){
		$this->oLayout->show();
	}
	
	
}
