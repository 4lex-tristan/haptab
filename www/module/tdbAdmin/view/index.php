<?php foreach ($this->tSimu as $oSimu): ?>
	<h2 class=sub-header"><?php echo $oSimu->name;?></h2>
	<table class="table">
	<?php foreach ($this->tGroupes[$oSimu->name] as $oGroupe):?>
		<tr>
			<th><?php echo $oGroupe->login?></th>
			<!-- <td><?php echo $this->tProgres[$oSimu->name][$oGroupe->login]['validated'].' & '.$this->tProgres[$oSimu->name][$oGroupe->login]['progress'];?></td> -->
			<td><div class="progress">
				<div class="progress-bar progress-bar-success" style="width: <?php echo $this->tProgres[$oSimu->name][$oGroupe->login]['validated'].'%'?>"></div> <!-- bar des suivis validé -->
				<?php $progressSup = $this->tProgres[$oSimu->name][$oGroupe->login]['progress'] -  $this->tProgres[$oSimu->name][$oGroupe->login]['validated'];?>
				<div class="progress-bar" style="width: <?php echo $progressSup.'%'?>"></div></div></td><!-- bar d'avancement general de la simulation -->
			<td><a class="btn btn-primary"
				href="<?php echo $this->getLink('detailequipe::index',array('id'=>$oGroupe->id)) ?>">Détail</a></td>
		</tr>
	<?php endforeach;?>
	</table>
<?php endforeach;?>