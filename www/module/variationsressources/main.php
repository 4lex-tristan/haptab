<?php 
class module_variationsressources extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tVariationsressources=model_variationsressources::getInstance()->findAll();
		
		$oView=new _view('variationsressources::list');
		$oView->tVariationsressources=$tVariationsressources;

		//$oView->tJoinmodel_suivi=model_datessuivi::getInstance()->getSelect();
		
		$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oVariationsressources=new row_variationsressources;
		
		$oView=new _view('variationsressources::new');
		$oView->oVariationsressources=$oVariationsressources;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oVariationsressources=model_variationsressources::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('variationsressources::edit');
		$oView->oVariationsressources=$oVariationsressources;
		$oView->tId=model_variationsressources::getInstance()->getIdTab();
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oVariationsressources=model_variationsressources::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('variationsressources::delete');
		$oView->oVariationsressources=$oVariationsressources;
		
				$oView->tJoinmodel_simulation=model_simulation::getInstance()->getSelect();

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oVariationsressources=new row_variationsressources;	
		}else{
			$oVariationsressources=model_variationsressources::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('simulation','suivi','niveau','rapidite','competence');
		foreach($tColumn as $sColumn){
			$oVariationsressources->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oVariationsressources->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('variationsressources::list');
		}else{
			return $oVariationsressources->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oVariationsressources=model_variationsressources::getInstance()->findById( _root::getParam('id',null) );
				
		$oVariationsressources->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('variationsressources::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

