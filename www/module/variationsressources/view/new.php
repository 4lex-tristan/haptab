<?php 
$oForm=new plugin_form($this->oVariationsressources);
$oForm->setMessage($this->tMessage);
?>
<form  class="form-horizontal" action="" method="POST" >

	
	<div class="form-group">
		<label class="col-sm-2 control-label">simulation</label>
		<div class="col-sm-10"><?php echo $oForm->getSelect('simulation',$this->tJoinmodel_simulation,array('class'=>'form-control'));?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">suivi</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('suivi',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('niveau',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">rapidit&eacute;</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('rapidite',array('class'=>'form-control'))?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">comp&eacute;tence</label>
		<div class="col-sm-10"><?php echo $oForm->getInputText('competence',array('class'=>'form-control'))?></div>
	</div>


<?php echo $oForm->getToken('token',$this->token)?>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input type="submit" class="btn btn-success" value="Modifier" /> <a class="btn btn-link" href="<?php echo $this->getLink('variationsressources::list')?>">Annuler</a>
	</div>
</div>
</form>
