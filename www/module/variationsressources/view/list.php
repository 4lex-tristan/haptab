<table class="table table-striped">
	<tr>
		
		<th>simulation</th>

		<th>suivi</th>

		<th>niveau</th>

		<th>rapidit&eacute;</th>

		<th>comp&eacute;tence</th>

		<th></th>
	</tr>
	<?php if($this->tVariationsressources):?>
		<?php foreach($this->tVariationsressources as $oVariationsressources):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php if(isset($this->tJoinmodel_simulation[$oVariationsressources->id_simulation])){ echo $this->tJoinmodel_simulation[$oVariationsressources->id_simulation];}else{ echo $oVariationsressources->id_simulation ;}?></td>

		<td><?php echo $oVariationsressources->id_suivi ?></td>

		<td><?php echo $oVariationsressources->niveau ?></td>

		<td><?php echo $oVariationsressources->rapidite ?></td>

		<td><?php echo $oVariationsressources->competence ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('variationsressources::edit',array(
										'id'=>$oVariationsressources->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('variationsressources::delete',array(
										'id'=>$oVariationsressources->getId()
									) 
							)?>">Delete</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="6">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('variationsressources::new') ?>">New</a></p>

