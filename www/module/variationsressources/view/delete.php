<form class="form-horizontal" action="" method="POST">


	<div class="form-group">
		<label class="col-sm-2 control-label">simulation</label>
		<div class="col-sm-10"><?php echo $this->tJoinmodel_simulation[$this->oVariationsressources->simulation]?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">suivi</label>
		<div class="col-sm-10"><?php echo $this->oVariationsressources->suivi ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">niveau</label>
		<div class="col-sm-10"><?php echo $this->oVariationsressources->niveau ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">rapidit&eacute;</label>
		<div class="col-sm-10"><?php echo $this->oVariationsressources->rapidite ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">comp&eacute;tence</label>
		<div class="col-sm-10"><?php echo $this->oVariationsressources->competence ?></div>
	</div>


<input type="hidden" name="token" value="<?php echo $this->token?>" />
<?php if($this->tMessage and isset($this->tMessage['token'])): echo $this->tMessage['token']; endif;?>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		<input class="btn btn-danger" type="submit" value="Confirmer la suppression" /> <a class="btn btn-link" href="<?php echo $this->getLink('variationsressources::list')?>">Annuler</a>
	</div>
</div>
</form>
