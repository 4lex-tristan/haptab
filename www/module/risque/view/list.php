<table class="table table-striped">
	<tr>
		
		<th>Nom du risque</th>

		<th>Description du risque</th>

		<th></th>
	</tr>
	<?php if($this->tRisque):?>
		<?php foreach($this->tRisque as $oRisque):?>
		<tr <?php echo plugin_tpl::alternate(array('','class="alt"'))?>>
			
		<td><?php echo $oRisque->nom ?></td>

		<td><?php echo $oRisque->description ?></td>

			<td>
				
				
<a class="btn btn-success" href="<?php echo $this->getLink('risque::edit',array(
										'id'=>$oRisque->getId()
									) 
							)?>">Edit</a>

<a class="btn btn-danger" href="<?php echo $this->getLink('risque::delete',array(
										'id'=>$oRisque->getId()
									) 
							)?>">Delete</a>

<a class="btn btn-default" href="<?php echo $this->getLink('risque::show',array(
										'id'=>$oRisque->getId()
									) 
							)?>">Show</a>

				
				
			</td>
		</tr>	
		<?php endforeach;?>
	<?php else:?>
		<tr>
			<td colspan="3">Aucune ligne</td>
		</tr>
	<?php endif;?>
</table>

<p><a class="btn btn-primary" href="<?php echo $this->getLink('risque::new') ?>">New</a></p>

