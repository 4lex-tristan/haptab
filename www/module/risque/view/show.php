<form class="form-horizontal" action="" method="POST" >
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nom du risque</label>
		<div class="col-sm-10"><?php echo $this->oRisque->nom ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">Description du risque</label>
		<div class="col-sm-10"><?php echo $this->oRisque->description ?></div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Mitigations</label>
		<div class="col-sm-10">
			<div class="table-responsive">
			<table class="table">
			<thead>
			<th>Niveau</th>
			<th>Intitulé</th>
			</thead>
			<?php $tMitigations=model_mitigationrisque::getInstance()->findAllByRisque( $this->oRisque->id_risque );
		foreach ($tMitigations as $oMitigation):?>
		<tr>
		<td><?php echo $oMitigation->niveau?></td>
		<td><a class="btn btn-link" href="<?php echo $this->getLink('mitigationrisque::show&id='.$oMitigation->id_mitigation)?>"><?php echo $oMitigation->intitule?></a></td>
		</tr>
		<?php endforeach;?>
		</table>
		</div>
		</div>
	</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
		 <a class="btn btn-default" href="<?php echo $this->getLink('risque::list')?>">Retour</a>
	</div>
</div>
</form>
