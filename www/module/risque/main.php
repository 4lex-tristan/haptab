<?php 
class module_risque extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	
	
	public function _index(){
	    //on considere que la page par defaut est la page de listage
	    $this->_list();
	}
	
	
	public function _list(){
		
		$tRisque=model_risque::getInstance()->findAll();
		
		$oView=new _view('risque::list');
		$oView->tRisque=$tRisque;
		
		
		
		$this->oLayout->add('main',$oView);
		 
	}

	
	
	public function _new(){
		$tMessage=$this->processSave();
	
		$oRisque=new row_risque;
		
		$oView=new _view('risque::new');
		$oView->oRisque=$oRisque;
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _edit(){
		$tMessage=$this->processSave();
		
		$oRisque=model_risque::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('risque::edit');
		$oView->oRisque=$oRisque;
		$oView->tId=model_risque::getInstance()->getIdTab();
		
		
		
		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _show(){
		$oRisque=model_risque::getInstance()->findById( _root::getParam('id') );
		
		
		$oView=new _view('risque::show');
		$oView->oRisque=$oRisque;
		
		
		$this->oLayout->add('main',$oView);
	}

	
	
	public function _delete(){
		$tMessage=$this->processDelete();

		$oRisque=model_risque::getInstance()->findById( _root::getParam('id') );
		
		$oView=new _view('risque::delete');
		$oView->oRisque=$oRisque;
		
		

		$oPluginXsrf=new plugin_xsrf();
		$oView->token=$oPluginXsrf->getToken();
		$oView->tMessage=$tMessage;
		
		$this->oLayout->add('main',$oView);
	}


	private function processSave(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$iId=_root::getParam('id',null);
		if($iId==null){
			$oRisque=new row_risque;	
		}else{
			$oRisque=model_risque::getInstance()->findById( _root::getParam('id',null) );
		}
		
		$tColumn=array('nom','description');
		foreach($tColumn as $sColumn){
			$oRisque->$sColumn=_root::getParam($sColumn,null) ;
		}
		
		
		if($oRisque->save()){
			//une fois enregistre on redirige (vers la page liste)
			_root::redirect('risque::list');
		}else{
			return $oRisque->getListError();
		}
		
	}
	
	
	public function processDelete(){
		if(!_root::getRequest()->isPost() ){ //si ce n'est pas une requete POST on ne soumet pas
			return null;
		}
		
		$oPluginXsrf=new plugin_xsrf();
		if(!$oPluginXsrf->checkToken( _root::getParam('token') ) ){ //on verifie que le token est valide
			return array('token'=>$oPluginXsrf->getMessage() );
		}
	
		$oRisque=model_risque::getInstance()->findById( _root::getParam('id',null) );
				
		$oRisque->delete();
		//une fois enregistre on redirige (vers la page liste)
		_root::redirect('risque::list');
		
	}


	
	public function after(){
		$this->oLayout->show();
	}
	
	
}

