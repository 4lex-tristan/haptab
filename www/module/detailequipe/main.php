<?php 
class module_detailequipe extends abstract_module{
	
	public function before(){
		_root::getAuth()->enable();
		if (! _root::getACL()->can('edit','all')){
			_root::redirect('tdbGroupe::index');
		}
		$this->oLayout=new _layout('bootstrap');
		$this->oLayout->addModule('menu','menuAdminAll::index');
	}
	/* #debutaction#
	public function _exampleaction(){
	
		$oView=new _view('examplemodule::exampleaction');
		
		$this->oLayout->add('main',$oView);
	}
	#finaction# */
	
	
	public function _index(){

		
		$oAccount=model_account::getInstance()->findById(_root::getParam('id'));
		$tSuivis=model_datessuivi::getInstance()->findBySimu($oAccount->id_simulation);
		$tRisques=model_risque::getInstance()->findAllBySimu($oAccount->id_simulation);
		foreach ($tSuivis as $oSuivi){
			$oResultat=model_resultat::getInstance()->findByContext($oAccount->id,$oAccount->id_simulation,$oSuivi->id);
			$tResultats[$oSuivi->id]=$oResultat;
			$tChoixComposants[$oSuivi->id]=model_choixcomposant::getInstance()->findByContext($oAccount->id,$oAccount->id_simulation,$oSuivi->id);
			$tTachesSimu[$oSuivi->id]=model_tachessimulees::getInstance()->findBySuivi($oAccount->id,$oAccount->id_simulation,$oSuivi->id);
			foreach ($tTachesSimu[$oSuivi->id] as $oTacheSimu){
				$tRH[$oSuivi->id][$oTacheSimu->id]['inge']=model_ingenieuraffecte::getInstance()->findAllByIdTache($oTacheSimu->id);
				$tRH[$oSuivi->id][$oTacheSimu->id]['tech']=model_technicienaffecte::getInstance()->findAllByIdTache($oTacheSimu->id);
				$tRH[$oSuivi->id][$oTacheSimu->id]['ouv']=$oTacheSimu->NbOuv;
			}
			foreach ($tRisques as $oRisque){
				$tMitigations[$oSuivi->id][$oRisque->id]=model_choixmitigation::getInstance()->findByContext($oAccount->id,$oAccount->id_simulation,$oSuivi->id,$oRisque->id);
			}
			
		}
		$tTaches=model_taches::getInstance()->findAllBySim($oAccount->id_simulation);
		
		//$tChoixComposants=model_choixcomposant::getInstance()->findById();
		
		
		$oView=new _view('detailequipe::index');
		$oView->idEquipe=$oAccount->id;
		$oView->tSuivis=$tSuivis;
		$oView->tResultats=$tResultats;
		$oView->tTaches=$tTaches;
		if (isset($tRH)){$oView->tRH=$tRH;}
		$oView->tRisques=$tRisques;
		if (isset($tMitigations)){$oView->tMitigations=$tMitigations;}
		$oView->tChoixComposants=$tChoixComposants;
		$oView->tJoinmodel_ComposantEcran=model_composant::getInstance()->getSelectEcran();
		$oView->tJoinmodel_ComposantProcesseur=model_composant::getInstance()->getSelectProcesseur();
		$oView->tJoinmodel_ComposantMemoire=model_composant::getInstance()->getSelectMemoire();
		$oView->tJoinmodel_ComposantWireless=model_composant::getInstance()->getSelectWireless();
		$oView->tJoinmodel_ComposantBattrie=model_composant::getInstance()->getSelectBatterie();
		$oView->tJoinmodel_ComposantCamera=model_composant::getInstance()->getSelectCamera();
		$oView->tJoinmodel_ComposantWebcam=model_composant::getInstance()->getSelectWebcam();
		$oView->tJoinmodel_ComposantGPS=model_composant::getInstance()->getSelectGPS();
		$oView->tJoinmodel_ComposantClavier=model_composant::getInstance()->getSelectClavier();
		$oView->tJoinmodel_ComposantBoitier=model_composant::getInstance()->getSelectBoitier();
		$oView->tJoinmodel_OS=model_systemeexplotation::getInstance()->getSelect();
		$oView->tJoinmodel_Ingenieur=model_ressourceshumaines::getInstance()->getSelectIng();
		$oView->tJoinmodel_Technicien = model_ressourceshumaines::getInstance ()->getSelectTech();
		$oView->tJoinmodel_Mitigations= model_mitigationrisque::getInstance()->getName();
		$this->oLayout->add('main',$oView);
	}
	
	
	public function after(){
		$this->oLayout->show();
	}
	
	
}
