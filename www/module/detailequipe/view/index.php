<div>
<a class="btn btn-link" href="<?php echo $this->getLink('tdbAdmin::index')?>">Retour liste équipe</a>
</div>
<h3 class="sub-header">Résultats</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i;?></th>
	<?php $i++; endforeach;?>
	</thead>
	<tr><th>Qualité</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->qualite_tot; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Cout</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->cout_tot; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Délai</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->delai_tot; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Score</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->score; }?></td>
	<?php  endforeach;?>
	</tr>
	
	</table>
</div>
<h3 class="sub-header">Détails Résultats</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i;?></th>
	<?php $i++; endforeach;?>
	</thead>
	<tr><th>Qualité matière première</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->qualite_mp; }?></td>
	<?php endforeach;?>
	</tr>
	<tr><th>Coût matière première</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->cout_mp; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Coût main d'oeuvre</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->cout_mo; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Durée sup. proto.</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->duree_sup_proto; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Durée sup. prod.</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->duree_sup_prod; }?></td>
	<?php endforeach;?>
	</tr>
	<tr><th>Poids</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->poids; }?></td>
	<?php  endforeach;?>
	</tr>
	<tr><th>Puissance</th>
	<?php  foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tResultats[$oSuivi->id])){ echo $this->tResultats[$oSuivi->id]->puissance; }?></td>
	<?php  endforeach;?>
	</tr>
	</table>
</div>
<h3 class="sub-header">Graphiques</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i;?></th>
	<?php $i++; endforeach;?>
	</thead>
	<tr>
	<th>Antécédences</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<th><?php if (isset($this->tResultats[$oSuivi->id]->delai_tot)):?>
	<a class="btn btn-primary" href="<?php echo $this->getLink('antecedent::index',array('id'=> $this->idEquipe,'suivi'=>$oSuivi->id))?>">Antécédent / PERT</a>
	<?php endif;?></th>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Gantt</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<th><?php if (isset($this->tResultats[$oSuivi->id]->delai_tot)):?>
	<a class="btn btn-primary" href="<?php echo $this->getLink('gantt::index',array('id'=> $this->idEquipe,'suivi'=>$oSuivi->id))?>">GANTT</a>
	<?php endif;?></th>
	<?php endforeach;?>
	</tr>
	</table>
</div>
<h3 class="sub-header">Composants</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i;?></th>
	<?php $i++; endforeach;?>
	</thead>
	<tr>
	<th>Ecran</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantEcran[$this->tChoixComposants[$oSuivi->id]->id_ecran];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Processeur</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantProcesseur[$this->tChoixComposants[$oSuivi->id]->id_processeur];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Mémoire</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantMemoire[$this->tChoixComposants[$oSuivi->id]->id_memoire];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Connexion sans fil</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantWireless[$this->tChoixComposants[$oSuivi->id]->id_wireless];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Batterie</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantBattrie[$this->tChoixComposants[$oSuivi->id]->id_battrie];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Caméra</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantCamera[$this->tChoixComposants[$oSuivi->id]->id_camera];} ?></td>
	<?php endforeach;?>
	</tr>
	<tr>
	<th>Webcam</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantWebcam[$this->tChoixComposants[$oSuivi->id]->id_webcam];} ?></td>
	<?php endforeach;?>
	</tr>
	<th>GPS</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantGPS[$this->tChoixComposants[$oSuivi->id]->id_gps];} ?></td>
	<?php endforeach;?>
	</tr>
	<th>Clavier</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantClavier[$this->tChoixComposants[$oSuivi->id]->id_clavier];} ?></td>
	<?php endforeach;?>
	</tr>
	<th>Boitier</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_ComposantBoitier[$this->tChoixComposants[$oSuivi->id]->id_boitier];} ?></td>
	<?php endforeach;?>
	</tr>
	<th>Système d'exploitation</th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tChoixComposants[$oSuivi->id])){echo $this->tJoinmodel_OS[$this->tChoixComposants[$oSuivi->id]->id_os];} ?></td>
	<?php endforeach;?>
	</tr>
	</table>
</div>
<h3 class="sub-header">Ressources Humaines</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i.': Ingé';?></th>
	<th><?php echo 'Suivi '.$i.': Tech';?></th>
	<th><?php echo 'Suivi '.$i.': Ouv';?></th>
	<?php $i++; endforeach;?>
	</thead>
	<?php foreach ($this->tTaches as $oTache):?>
	<tr>
	<th><?php echo $oTache->Nom ?></th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tRH) && isset($this->tRH[$oSuivi->id]) && isset($this->tRH[$oSuivi->id][$oTache->id])){
		foreach ($this->tRH[$oSuivi->id][$oTache->id]['inge'] as $oInge){
			echo $this->tJoinmodel_Ingenieur[$oInge->IdIng].' ';}
	}?></td>
	<td><?php  if (isset($this->tRH) && isset($this->tRH[$oSuivi->id]) && isset($this->tRH[$oSuivi->id][$oTache->id]) ){
		foreach ($this->tRH[$oSuivi->id][$oTache->id]['tech'] as $oTech){
			echo $this->tJoinmodel_Technicien[$oTech->IdTech].' ';}
	}?></td>
	<td><?php  if (isset($this->tRH) && isset($this->tRH[$oSuivi->id]) && isset($this->tRH[$oSuivi->id][$oTache->id])){ 
		echo $this->tRH[$oSuivi->id][$oTache->id]['ouv'];}?></td>
	<?php  endforeach;?>
	</tr>
	<?php endforeach;?>
	</table>
</div>
<h3 class="sub-header">Mitigations</h3>
<div class="table-responsive">
	<table class="table">
	<thead>
	<th></th>
	<?php $i=0; foreach ($this->tSuivis as $oSuivi):?>
	<th><?php echo 'Suivi '.$i;?></th>
	<?php $i++; endforeach;?>
	</thead>
	<?php foreach ($this->tRisques as $oRisque):?>
	<tr>
	<th><?php echo $oRisque->nom?></th>
	<?php foreach ($this->tSuivis as $oSuivi):?>
	<td><?php if (isset($this->tMitigations[$oSuivi->id][$oRisque->id_risque])){
		echo $this->tJoinmodel_Mitigations[$this->tMitigations[$oSuivi->id][$oRisque->id_risque]->id_mitigation] ;
	}?>
	<?php endforeach;?>
	</tr>
	<?php endforeach;?>
	</table>
</div>
<div>
<a class="btn btn-link" href="<?php echo $this->getLink('tdbAdmin::index')?>">Retour liste équipe</a>
</div>