<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#">HapTab</a>
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav nav">
				<?php foreach($this->tLink as $sLibelle => $sLink): ?>
					<?php if(_root::getParamNav()==$sLink):?>
						<li class="active nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLink) ?>"><?php echo $sLibelle ?></a></li>
					<?php else:?>
						<li class="nav-item"><a class="nav-link" href="<?php echo $this->getLink($sLink) ?>"><?php echo $sLibelle ?></a></li>
					<?php endif;?>
				<?php endforeach;?>
			</ul>
			<form class="navbar-form navbar-right">
				<?php foreach($this->tLink2 as $sLibelle2 => $sLink2): ?>
					<button href="<?php echo $this->getLink($sLink2) ?>" class="btn btn-secondary my-2 my-sm-0" type="submit" ><?php echo $sLibelle2 ?></button>
				<?php endforeach;?>
			</form>
		</div>
	</div>
</nav>

