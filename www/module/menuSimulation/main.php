<?php
Class module_menuSimulation extends abstract_moduleembedded{
		
	public function _index(){
		
		$tLink=array(
			'Tableau de bord' => 'tdbGroupe::index',
			'Evènements'=>'choixcorrection::index',
			'Risques' => 'choixmitigation::index',
			'Composants Tablette' => 'choixcomposant::index',
			'Taches' => 'tachessimulees::index'
		);
		$tLink2=array(
				'Log out' => 'auth::logout',
		);
		
		$oView=new _view('menuSimulation::index');
		$oView->tLink=$tLink;
		$oView->tLink2=$tLink2;
		
		return $oView;
	}
}
