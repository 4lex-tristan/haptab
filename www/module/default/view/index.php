<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">HapTab</a>
		</div>
		<div class="collapse navbar-collapse">

			<form class="navbar-form navbar-right"> 
        <a href="<?php echo $this->getLink('auth::login') ?>" class="btn btn-primary my-2 my-sm-0" type="submit">Connexion</a>
			</form>
		</div><!--/.nav-collapse -->
	</div>
</nav>


<div class="container">
    <div class="jumbotron">
      <h1 class="display-3">HapTab</h1>
      <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
    </div>
  </div>