<?php
class model_parametrevalidation extends abstract_model{
	
	protected $sClassRow='row_parametrevalidation';
	
	protected $sTable='parametrevalidation';
	protected $sConfig='prospectapp';
	
	protected $tId=array('id');

	public static function getInstance(){
		return self::_getInstance(__CLASS__);
	}

	public function findById($uId){
		return $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id=?',$uId );
	}
	public function findAll(){
		return $this->findMany('SELECT * FROM '.$this->sTable);
	}
	public function findByContext($idEquipe,$idSimu,$suivi){
		return $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
	}
	public function strategyIsValidated($idEquipe,$idSimu,$suivi){
		$oValid= $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
		if($oValid != null && $oValid->strategy_valid != null && $oValid->strategy_valid == 1){
			return TRUE;
		}else {
			return FALSE;
		}
	}
	public function eventIsValidated($idEquipe,$idSimu,$suivi){
		$oValid= $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
		if($oValid != null && $oValid->event_valid != null && $oValid->event_valid == 1){
			return TRUE;
		}else {
			return FALSE;
		}
	}
	public function tabletIsValidated($idEquipe,$idSimu,$suivi){
		$oValid= $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
		if($oValid != null && $oValid->tablette_valid != null && $oValid->tablette_valid == 1){
			return TRUE;
		}else {
			return FALSE;
		}
	}
	public function tasksAreValidated($idEquipe,$idSimu,$suivi){
		$oValid= $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
		if($oValid != null && $oValid->tache_valid != null && $oValid->tache_valid == 1){
			return TRUE;
		}else {
			return FALSE;
		}
	}
	public function riskIsValidated($idEquipe,$idSimu,$suivi){
		$oValid= $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id_equipe=? AND id_simu=? AND id_suivi=? ',array($idEquipe,$idSimu,$suivi) );
		if($oValid != null && $oValid->risque_valid != null && $oValid->risque_valid == 1){
			return TRUE;
		}else {
			return FALSE;
		}
	}
}

class row_parametrevalidation extends abstract_row{
	
	protected $sClassModel='model_parametrevalidation';
	
	/*exemple jointure 
	public function findAuteur(){
		return model_auteur::getInstance()->findById($this->auteur_id);
	}
	*/
	/*exemple test validation*/
	private function getCheck(){
		$oPluginValid=new plugin_valid($this->getTab());
		
		
		/* renseigner vos check ici
		$oPluginValid->isEqual('champ','valeurB','Le champ n\est pas &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isNotEqual('champ','valeurB','Le champ est &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isUpperThan('champ','valeurB','Le champ n\est pas sup&eacute; &agrave; '.$valeurB);
		$oPluginValid->isUpperOrEqualThan('champ','valeurB','Le champ n\est pas sup&eacute; ou &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isLowerThan('champ','valeurB','Le champ n\est pas inf&eacute;rieur &agrave; '.$valeurB);
		$oPluginValid->isLowerOrEqualThan('champ','valeurB','Le champ n\est pas inf&eacute;rieur ou &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isEmpty('champ','Le champ n\'est pas vide');
		$oPluginValid->isNotEmpty('champ','Le champ ne doit pas &ecirc;tre vide');
		$oPluginValid->isEmailValid('champ','L\email est invalide');
		$oPluginValid->matchExpression('champ','/[0-9]/','Le champ n\'est pas au bon format');
		$oPluginValid->notMatchExpression('champ','/[a-zA-Z]/','Le champ ne doit pas &ecirc;tre a ce format');
		*/

		return $oPluginValid;
	}

	public function isValid(){
		return $this->getCheck()->isValid();
	}
	public function getListError(){
		return $this->getCheck()->getListError();
	}
	public function save(){
		if(!$this->isValid()){
			return false;
		}
		parent::save();
		return true;
	}

}
