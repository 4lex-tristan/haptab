<?php
class model_composant extends abstract_model{
	
	protected $sClassRow='row_composant';
	
	protected $sTable='composant';
	protected $sConfig='prospectapp';
	
	protected $tId=array('id');

	public static function getInstance(){
		return self::_getInstance(__CLASS__);
	}

	public function findById($uId){
		return $this->findOne('SELECT * FROM '.$this->sTable.' WHERE id=?',$uId );
	}
	public function findAll(){
		return $this->findMany('SELECT * FROM '.$this->sTable);
	}
	public function getSelectEcran(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Ecran tactile"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectProcesseur(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Processeur"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectMemoire(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Memoire"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectWireless(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Connexion sans fil"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectBatterie(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Batterie"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectCamera(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Camera"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectWebcam(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Webcam"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectGPS(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="GPS"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectClavier(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Clavier"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	public function getSelectBoitier(){
		$tab=$this->findMany('SELECT * FROM '.$this->sTable.' WHERE type="Boitier"');
		$tSelect=array();
		if($tab){
			foreach($tab as $oRow){
				$tSelect[ $oRow->id ]=$oRow->nom;
			}
		}
		return $tSelect;
	}
	
	
	
}

class row_composant extends abstract_row{
	
	protected $sClassModel='model_composant';
	
	/*exemple jointure 
	public function findAuteur(){
		return model_auteur::getInstance()->findById($this->auteur_id);
	}
	*/
	/*exemple test validation*/
	private function getCheck(){
		$oPluginValid=new plugin_valid($this->getTab());
		
		
		/* renseigner vos check ici
		$oPluginValid->isEqual('champ','valeurB','Le champ n\est pas &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isNotEqual('champ','valeurB','Le champ est &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isUpperThan('champ','valeurB','Le champ n\est pas sup&eacute; &agrave; '.$valeurB);
		$oPluginValid->isUpperOrEqualThan('champ','valeurB','Le champ n\est pas sup&eacute; ou &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isLowerThan('champ','valeurB','Le champ n\est pas inf&eacute;rieur &agrave; '.$valeurB);
		$oPluginValid->isLowerOrEqualThan('champ','valeurB','Le champ n\est pas inf&eacute;rieur ou &eacute;gal &agrave; '.$valeurB);
		$oPluginValid->isEmpty('champ','Le champ n\'est pas vide');
		$oPluginValid->isNotEmpty('champ','Le champ ne doit pas &ecirc;tre vide');
		$oPluginValid->isEmailValid('champ','L\email est invalide');
		$oPluginValid->matchExpression('champ','/[0-9]/','Le champ n\'est pas au bon format');
		$oPluginValid->notMatchExpression('champ','/[a-zA-Z]/','Le champ ne doit pas &ecirc;tre a ce format');
		*/

		return $oPluginValid;
	}

	public function isValid(){
		return $this->getCheck()->isValid();
	}
	public function getListError(){
		return $this->getCheck()->getListError();
	}
	public function save(){
		if(!$this->isValid()){
			return false;
		}
		parent::save();
		return true;
	}

}
