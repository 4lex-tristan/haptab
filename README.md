# haptab

PHP web app for a project management simulation. It is a workable version of the app of 2015.

## Database 
A copy of the 2015 database in is 'propsect-2.sql'. It should be loaded in phpmyadmin before the first use.

## Start servers in docker
'''
docker-compose up --build 
'''

## Access 
Once 'docker-compose up' has been done, different services are availabe throught:
* [PHPmyAdmin](localhost:8080)
* [HapTab](localhost:8100)